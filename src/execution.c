/* execution.c
 * execution manager for trading system
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2020 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file execution.c
 * @brief Routines for signal and stop loss execution
 *
 * This file contains the routines responsible to execute orders and stop 
 * losses. Execution is hereby defined within the boundaries of the program (in
 * the database and terminal output) it does not include relaying orders to a
 * broker and executing them in real life
 * @author Denis Zetzmann
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include "debug.h"
#include "constants.h"
#include "execution.h"
#include "database.h"
#include "readconf.h"
#include "arrays.h"
#include "date.h"
#include "class_signals.h"
#include "class_signal_list.h"
#include "class_stoploss_list.h"
#include "stoploss.h"
#include "datatypes.h"

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief execute stop losses for a given date and market
 *
 * This function takes all markets, the orderbook and portfolio and checks all
 * active portfolio positions against their current stop losses and executes 
 * them, if hit.
 * @todo: think again through all combinations of 
 * config parameters like SIGNAL_EXECUTION_DATE, SL_TYPE, SL_ADJUST and
 * so on, can actual implementation handle them all correctly?
 * @todo: Speedup: move mysql_get_single_quote out of the loop!
 * 
 * @param account pointer to account object
 * @param markets pointer to market_list object containing all markets 
 * @param portfolio pointer to current portfolio object
 * @param orderbook pointer to orderbook objekt
 * @param daynr uint with current date as daynr 
 * @return 0 for success, 1 for error
 */
///////////////////////////////////////////////////////////////////////////////
int execute_stop_loss(class_accounts *account, class_market_list* markets, 
                      class_portfolio* portfolio, class_orderbook* orderbook,
                      unsigned int daynr, class_stoploss_list* sl_list)
{
	extern parameter parms; // parms are declared global in main.c
 	extern bool verbose_flag; // declared global in main.c
 	
 	// get the number of portfolio entries
 	unsigned int nr_entries = portfolio->getNrElements(portfolio);
 	
    unsigned int max_entries = nr_entries;
    
    for(unsigned int entry_idx = 0; entry_idx < max_entries; entry_idx++)
    {
        //TODO: CHECK FOR -1 in returned indices
        // look in market list for position of current portfolio entrie´s market
        int market_idx = markets->getElementIndex(markets, bdata(*portfolio->elements[entry_idx]->symbol));
        check(market_idx>=0, "symbol %s from portfolio not found in market list %s", error_market, bdata(*portfolio->elements[entry_idx]->symbol), bdata(*markets->name));
        
        // find position of "indicator" close within this market
        int close_idx = markets->markets[market_idx]->getIndicatorPos(markets->markets[market_idx], "close");
        
        // find position of current daynr within close quotes of the market
        int quote_idx = markets->markets[market_idx]->Ind[close_idx]->QuoteObj->findDaynrIndexlB(markets->markets[market_idx]->Ind[close_idx]->QuoteObj, daynr);
        
        // now get the close quote for this market with the given daynr
        float close_quote = markets->markets[market_idx]->Ind[close_idx]->QuoteObj->quotevec[quote_idx];
   
        float stoploss = portfolio->elements[entry_idx]->stoploss;
       
        bool should_be_executed = false;
        switch(portfolio->elements[entry_idx]->type)
        {
            case longsignal:
                if(close_quote < stoploss)
                    should_be_executed = true;
                break;
            case shortsignal:
                if(close_quote > stoploss)
                    should_be_executed = true;
                break;
            case undefined:
                break;
        }
        if(should_be_executed)
        {
            // calculate transaction cost
            float fee = calculate_fee(portfolio->elements[entry_idx]);
            
            // add a sellorder entry to orderbook
            orderbook->addNewElement(orderbook, portfolio->elements[entry_idx], sellorder, fee);

            // substract transaction cost and update account cash
            float cash = account->getCash(account);
            account->setCash(account, cash + portfolio->elements[entry_idx]->current_value - fee);
            account->sum_fees = account->sum_fees + fee;
            
            
            bstring typestring = NULL;
            switch(portfolio->elements[entry_idx]->type)
            {
                case longsignal:
                        typestring = bfromcstr("long");
                        break;
                case shortsignal:
                        typestring = bfromcstr("short");
                        break;
                case undefined:
                        break;            
            }
            printf(ANSI_COLOR_CYAN"\n[SL hit] %s %s (%s) buydate %s, %s@%.*f, PL ", 
                   bdata(markets->markets[market_idx]->Ind[close_idx]->QuoteObj->datevec[quote_idx]), 
                   bdata(*markets->markets[market_idx]->symbol),
                   bdata(*markets->markets[market_idx]->identifier),
                   bdata(*portfolio->elements[entry_idx]->Buyquote_market->datevec),
                   bdata(typestring), 
                   markets->markets[market_idx]->Ind[close_idx]->QuoteObj->nr_digits,
                   stoploss);
            
            if(portfolio->elements[entry_idx]->p_l < 0)
                printf(ANSI_COLOR_RED"%.*f"ANSI_COLOR_RESET, account->nr_digits, portfolio->elements[entry_idx]->p_l);
            else
                printf(ANSI_COLOR_YELLOW"%.*f"ANSI_COLOR_RESET, account->nr_digits, portfolio->elements[entry_idx]->p_l);
            
            // update SL record
            unsigned int sl_idx = portfolio->elements[entry_idx]->getSL_list_idx(portfolio->elements[entry_idx]);
            *sl_list->SL[sl_idx]->selldate = bstrcpy(markets->markets[market_idx]->Ind[close_idx]->QuoteObj->datevec[quote_idx]);
           
            // now remove the element from the list
            portfolio->removeElement(portfolio, entry_idx);    
            
            // update portfolio equity
            portfolio->updatePortfolioEquity(portfolio);
            
            // update account equity
            account->setEquity(account, portfolio->getPortfolioEquity(portfolio));
            account->setRiskFreeEquity(account, portfolio->getPortfolioRiskFreeEquity(portfolio));
            
            max_entries--;
            entry_idx--;    // decrease by one otherweise we might hit the exit condition without checking the new last element
            printf("--> executed! new Cash: %.*f %s", account->nr_digits, account->getCash(account), bdata(*account->currency));
            if(verbose_flag)
                printf(" (fee: %.*f)", parms.ACCOUNT_CURRENCY_DIGITS, fee);
            bdestroy(typestring);
        }
    }

    return EXIT_SUCCESS;
    
error_market:
    // cleanup what is possible
    account->destroy(account);
    markets->destroy(markets);
    portfolio->destroy(portfolio);
    orderbook->destroy(orderbook);
    exit(EXIT_FAILURE);
}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief central execution algorithm for all markets, performs stop losses and
 *        signal execution
 *
 * This function goes through all markets for the configured 
 * parms.SIGNAL_DAYS_TO_EXECUTE and executes stop losses and new signals. This 
 * function uses all other function in execution.c to perform this task. The 
 * execution decision is based on the configuration, e.g. on parameters like
 * parms.SIGNAL_EXECUTION_LONG/ parms.SIGNAL_EXECUTION_SHORT and so on.
 * For the meaning of the parameters ticks...symboltype see the symbols file
 * configuration.
 * 
 * @return 0 if successful, 1 otherwise
 */
////////////////////////////////////////////////////////////////////////////////
int execution_manager(class_accounts *the_account, class_market_list *market_list, class_signal_list *the_signals, class_portfolio *the_portfolio, class_orderbook *orderbook)
{
	extern parameter parms; // parms are declared global in main.c
	extern bool verbose_flag; // declared global in main.c
   
    // =================================================================
    // prepare execution phase
    // =================================================================
    
    unsigned int nr_markets = market_list->getNrElements(market_list);
    
    int closePos[nr_markets];   // stores the position of the close indicator for each market
    for(unsigned i=0; i< nr_markets; i++)
    {
        closePos[i] = market_list->markets[i]->getIndicatorPos(market_list->markets[i], "close");
        check(closePos[i] >=0, "Indicator \"close\" does not exist in market object \"%s\"", error_quote, bdata(*market_list->markets[i]->symbol));
    }
	
    // lowest/highest daynrs of all markets, init with first market
    // both values determine the execution period
	unsigned int startdaynr = market_list->markets[0]->Ind[closePos[0]]->DAYNRVEC[market_list->markets[0]->Ind[closePos[0]]->QuoteObj->nr_elements - parms.SIGNAL_DAYS_TO_EXECUTE];
    unsigned int enddaynr = market_list->markets[nr_markets-1]->Ind[closePos[nr_markets-1]]->DAYNRVEC[market_list->markets[nr_markets-1]->Ind[closePos[0]]->QuoteObj->nr_elements-1];

    // filter signal list for signals within execution period
    class_signal_list* tmp_signals_filtered_daynr = NULL;
    tmp_signals_filtered_daynr = class_signal_list_init("filtered by execution period");
    the_signals->filterOutSignalsBeforeDaynr(the_signals, tmp_signals_filtered_daynr, startdaynr);
    
	// now filter signal list for the configured strengths
    class_signal_list* tmp_signals_filtered_strength = NULL;
	tmp_signals_filtered_strength = class_signal_list_init("sorted and filtered signals in execution period");
	
	if(parms.SIGNAL_EXECUTION_WEAK)
		the_signals->filterSignalsPerStrength(tmp_signals_filtered_daynr, tmp_signals_filtered_strength, weak);
	if(parms.SIGNAL_EXECUTION_NEUTRAL)
		the_signals->filterSignalsPerStrength(tmp_signals_filtered_daynr, tmp_signals_filtered_strength, neutral);
	if(parms.SIGNAL_EXECUTION_STRONG)
		the_signals->filterSignalsPerStrength(tmp_signals_filtered_daynr, tmp_signals_filtered_strength, strong);
		
	// now filter signal list for configured trend type (long/short)
	class_signal_list* tmp_signals_filtered = NULL;
	if(parms.SIGNAL_EXECUTION_LONG && parms.SIGNAL_EXECUTION_SHORT)
		tmp_signals_filtered = tmp_signals_filtered_strength->clone(tmp_signals_filtered_strength);
	else if (parms.SIGNAL_EXECUTION_LONG && !parms.SIGNAL_EXECUTION_SHORT)
	{
		tmp_signals_filtered = class_signal_list_init("filtered by strength and trendtype in execution period");
		tmp_signals_filtered_strength->filterSignalsPerTrendtype(tmp_signals_filtered_strength, tmp_signals_filtered, longsignal);
	}
	else if (!parms.SIGNAL_EXECUTION_LONG && parms.SIGNAL_EXECUTION_SHORT)
	{
		tmp_signals_filtered = class_signal_list_init("filtered by strength and trendtype in execution period");
		tmp_signals_filtered_strength->filterSignalsPerTrendtype(tmp_signals_filtered_strength, tmp_signals_filtered, shortsignal);
	}
	else
		tmp_signals_filtered = class_signal_list_init("empty list");
 
	// now filter for nr of daily signals in one market: if parms.SIGNAL_EXECUTION_PYRAMID
	// is set to daily, only 1 signal per market will be executed each day
	class_signal_list* tmp_signals_pyramid = NULL;
	if(biseqcstr(parms.SIGNAL_EXECUTION_PYRAMID, "daily"))
	{
		tmp_signals_pyramid = class_signal_list_init("filtered by strength, trendtype and single signals per market and day (in execution period)");
		tmp_signals_filtered->filterOutMultipleSignalsADay(tmp_signals_filtered, tmp_signals_pyramid);
	}
	else
		tmp_signals_pyramid = tmp_signals_filtered->clone(tmp_signals_filtered);
	
	// order signals by daynrs
    class_signal_list* signals_sorted = NULL;
    signals_sorted = tmp_signals_pyramid->getSortedListByDaynr(tmp_signals_pyramid);

	// free some memory as we won't need the temporary lists any longer
	tmp_signals_filtered_daynr->destroy(tmp_signals_filtered_daynr);
	tmp_signals_filtered_strength->destroy(tmp_signals_filtered_strength);
    tmp_signals_filtered->destroy(tmp_signals_filtered);
    tmp_signals_pyramid->destroy(tmp_signals_pyramid);

    // setup performance tracking, (ab)use class indicators for that
    class_indicators* performance_total = NULL;
    class_indicators* performance_cash = NULL;
    class_indicators* performance_equity = NULL;
    class_indicators* performance_rf_equity = NULL;
    class_indicators* performance_total_high = NULL;
    
    performance_total = class_indicators_init("performance", "performance", enddaynr - startdaynr + 1, parms.ACCOUNT_CURRENCY_DIGITS, "total", "total performance (cash+equity)", "performance_record");
    performance_cash = class_indicators_init("performance", "performance", enddaynr - startdaynr + 1, parms.ACCOUNT_CURRENCY_DIGITS, "cash", "available free cash", "performance_record");
    performance_equity = class_indicators_init("performance", "performance", enddaynr - startdaynr + 1, parms.ACCOUNT_CURRENCY_DIGITS, "equity", "equity in portfolio", "performance_record"); 
    performance_rf_equity = class_indicators_init("performance", "performance", enddaynr - startdaynr + 1, parms.ACCOUNT_CURRENCY_DIGITS, "risk_free_equity", "equity @ stop loss", "performance_record");
    performance_total_high = class_indicators_init("performance", "performance", enddaynr - startdaynr + 1, parms.ACCOUNT_CURRENCY_DIGITS, "total_high", "total high watermark", "performance_record");
    
    performance_total->db_saveflag = true;
    performance_cash->db_saveflag = true;
    performance_equity->db_saveflag = true;
    performance_rf_equity->db_saveflag = true;
    performance_total_high->db_saveflag = true;

    unsigned int performance_idx = 0;
    
    // set up a stop loss list to track all SL
    class_stoploss_list* sl_list = NULL;
    sl_list = class_stoploss_list_init("stop loss records");

    // loop through elements in portfolio and create corresponding SL records
    for(unsigned idx = 0; idx < the_portfolio->getNrElements(the_portfolio); idx++)
    {
        sl_list->addNewSL(sl_list, bdata(*the_portfolio->elements[idx]->symbol), 1, the_portfolio->elements[idx]->Buyquote_account->nr_digits, bdata(the_portfolio->elements[idx]->Buyquote_account->datevec[0]), bdata(parms.SL_TYPE));
        sl_list->SL[sl_list->nr_records-1]->QuoteObj->setQuoteTupel(sl_list->SL[sl_list->nr_records-1]->QuoteObj, 0, 
                                                        &the_portfolio->elements[idx]->Buyquote_market->datevec[0], 
                                                        the_portfolio->elements[idx]->Buyquote_market->daynrvec[0], 
                                                        the_portfolio->elements[idx]->stoploss);
        the_portfolio->elements[idx]->setSL_list_idx(the_portfolio->elements[idx], idx);
    }
    
// =====================================================================
// End of preparation, now execute signals
// =====================================================================
    
    
    // Loop through all days of execution period
	// -----------------------------------------------------------------	
	for(unsigned int current_day = startdaynr; current_day <= enddaynr; current_day++)
	{	
		// Update portfolio entries with latest prices, SL etc. --------
		for(unsigned int i = 0; i < the_portfolio->getNrElements(the_portfolio); i++)
		{
			const int market_idx = market_list->getElementIndex(market_list, bdata(*the_portfolio->elements[i]->symbol));
            check(market_idx>=0, "symbol %s from portfolio not found in market list %s", error_market, bdata(*the_portfolio->elements[i]->symbol), bdata(*market_list->name));
            
			const unsigned int ind_index = market_list->markets[market_idx]->IndPos.close;
			// ok, the following IS ugly. translation: get me the index 
			// of the given daynr within following quote object:
			// market with current symbol, within this market the Indicator 
			// named close, within this indicator the said QuoteObj
			const unsigned int quote_idx = market_list->markets[market_idx]->Ind[ind_index]->QuoteObj->findDaynrIndexlB(market_list->markets[market_idx]->Ind[ind_index]->QuoteObj, current_day);
			//printf("\n daynr: %i, market idx: %i\t quote idx: %i", current_day, market_idx, quote_idx);
			
            // check for each portfolio entry if daynr of last quote is less than current daynr 
            // (which means it was already updated during a former program run)
            if(the_portfolio->elements[i]->currQuote_market->daynrvec[0] >= current_day)
                continue; // neccessary to prevent counting up trading days when running program again
            
            const unsigned int tradingdays = the_portfolio->elements[i]->trading_days + 1;

            float new_sl = new_stop_loss(market_list->markets[market_idx], quote_idx, the_portfolio->elements[i]->type);
            
            // catch some exceptions when SL should not be updated
            if(biseqcstr(parms.SL_ADJUST, "fixed"))
            {
                new_sl = the_portfolio->elements[i]->stoploss;
            }
            else if(biseqcstr(parms.SL_ADJUST, "trailing"))
            {
				if(!new_sl_needed(new_sl, the_portfolio->elements[i]->stoploss, the_portfolio->elements[i]->type))
                    new_sl = the_portfolio->elements[i]->stoploss;
            } 
            
            // update the SL in portfolio element
            the_portfolio->elements[i]->update(the_portfolio->elements[i],
                            new_sl, false, tradingdays, 
                            market_list->markets[market_idx], quote_idx, the_account->getCash(the_account));
            
            // update SL records
            unsigned int sl_idx = the_portfolio->elements[i]->getSL_list_idx(the_portfolio->elements[i]);
            sl_list->SL[sl_idx]->QuoteObj->appendQuoteTupel(sl_list->SL[sl_idx]->QuoteObj, 
                                                            &market_list->markets[market_idx]->Ind[ind_index]->QuoteObj->datevec[quote_idx], 
                                                            market_list->markets[market_idx]->Ind[ind_index]->QuoteObj->daynrvec[quote_idx], 
                                                            new_sl);
            
        } // update portfolio entries loop

       // EXECUTE stop losses for given day ---------------------------
       if(the_portfolio->getNrElements(the_portfolio))
        {
            //  printf("\n\n%i ######################### SL LOOP ##############################", current_day);
            execute_stop_loss(the_account, market_list, the_portfolio, orderbook, current_day, sl_list);   
        }
        
        // Update statistics for all markets (the method is intelligent
        // enough to check if the update is due wrt to configuration
        for(unsigned int i= 0; i< market_list->getNrElements(market_list); i++)
        {
			int quote_idx = market_list->markets[i]->Ind[0]->QuoteObj->findDaynrIndex(market_list->markets[i]->Ind[0]->QuoteObj, current_day);
			if(quote_idx < 0)
				continue;
			
            bool updated = market_list->markets[i]->updateStatistics(market_list->markets[i],quote_idx);
            if(updated) // update Covariance Matrix only if any´s market statistic changed
                market_list->updateStatistics(market_list);
		}
		
		// create a sublist of signals for current day
		class_signal_list* daily_signals = NULL;
		daily_signals = class_signal_list_init("Signals per day");
		daily_signals->filterSignalsPerDaynr(signals_sorted, daily_signals, current_day);
 
		// Loop through all signals for a given day
		// -------------------------------------------------------------
		for(unsigned int current_signal = 0; current_signal < daily_signals->nr_signals; current_signal++)
		{
			unsigned int marketPos = market_list->getElementIndex(market_list, bdata(*daily_signals->Sig[current_signal]->THE_SYMBOL));
            
            bool skip_signal = false;
            bstring skip_reason = NULL;
            
            // if the signal (that was pulled from database) already was executed --> go to next
            if(daily_signals->Sig[current_signal]->executed)
            {
                skip_reason = bfromcstr("(already executed)");
                skip_signal = true;
            }
		
			// check if market is configured as tradeable
			if(!market_list->markets[marketPos]->tradeable)
			{
				// this usually should not happen here as for non tradeable
				// markets no signals are generated. However, if a list of
				// signals is loaded from the db (from a former program run)
				// non tradeable markets could still slip in
                skip_reason = bfromcstr("configured as not tradeable");
				skip_signal = true;
			}
		
			// check if pyramiding is allowed and at least 1 position already exists	
			if((the_portfolio->containsSymbol(the_portfolio,
					market_list->markets[marketPos]->symbol)) && biseqcstr(parms.SIGNAL_EXECUTION_PYRAMID, "none"))
			{
                skip_reason = bfromcstr("due to SIGNAL_EXECUTION_PYRAMID = none");
				skip_signal = true;
			}	
								
			int market_daynr_idx = -1;
			const int market_closePos = market_list->markets[marketPos]->IndPos.close;
			const int market_openPos = market_list->markets[marketPos]->IndPos.open;
			
            if(biseqcstr(parms.SIGNAL_EXECUTION_DATE, "real_date"))
			{
				market_daynr_idx = market_list->markets[marketPos]->Ind[market_openPos]->QuoteObj->nr_elements - 1;
                check(market_daynr_idx >=0, "no quote index nr. %i in quote %s in market %s!", 
                      error_market_daynr_idx, market_daynr_idx, 
                      bdata(*market_list->markets[marketPos]->Ind[market_openPos]->QuoteObj->quotetype), 
                      bdata(*market_list->markets[marketPos]->symbol));
			}
			else if(biseqcstr(parms.SIGNAL_EXECUTION_DATE, "signal_date"))
			{
				market_daynr_idx = market_list->markets[marketPos]->Ind[market_closePos]->QuoteObj->findDaynrIndexlB(market_list->markets[marketPos]->Ind[market_closePos]->QuoteObj, *daily_signals->Sig[current_signal]->SIG_DAYNR);
                check(market_daynr_idx >=0, "no quote for daynr %i found within %s in market %s!", 
                      error_market_daynr_idx, *daily_signals->Sig[current_signal]->SIG_DAYNR,  
                      bdata(*market_list->markets[marketPos]->Ind[market_openPos]->QuoteObj->quotetype), 
                      bdata(*market_list->markets[marketPos]->symbol));
			}
			else if(biseqcstr(parms.SIGNAL_EXECUTION_DATE, "signal_next"))
			{
				market_daynr_idx = market_list->markets[marketPos]->Ind[market_openPos]->QuoteObj->findDaynrIndex(market_list->markets[marketPos]->Ind[market_openPos]->QuoteObj, *daily_signals->Sig[current_signal]->SIG_DAYNR) + 1;
                check(market_daynr_idx >=0, "no quote for daynr %i found within %s in market %s!", 
                      error_market_daynr_idx, *daily_signals->Sig[current_signal]->SIG_DAYNR,  
                      bdata(*market_list->markets[marketPos]->Ind[market_openPos]->QuoteObj->quotetype), 
                      bdata(*market_list->markets[marketPos]->symbol));
			}
            
			// check if current market regime allows execution of signals
			if(!biseqcstr(parms.SIGNAL_REGIME_FILTER, "none"))
            {
                float regime_filter_today = 0;
                const int regPos = market_list->markets[marketPos]->getIndicatorPos(market_list->markets[marketPos], "regime_filter");
                regime_filter_today = market_list->markets[marketPos]->Ind[regPos]->QuoteObj->quotevec[market_daynr_idx];
                
                if(regime_filter_today)
                {
                    skip_reason = bfromcstr("due to filtered by SIGNAL_REGIME_FILTER");
                    skip_signal = true;                   
                }
            }
		
			// find out which weekday the signal occured
            bstring weekday = NULL;
			get_weekday(&weekday, *daily_signals->Sig[current_signal]->SIG_DAYNR);
			bool current_day_sunday = false;
			bool current_day_saturday = false;
			if(biseqcstr(weekday, "Sunday"))
				current_day_sunday = true;
			if(biseqcstr(weekday, "Saturday"))
				current_day_saturday = true;

            bdestroy(weekday);

            // execution on sundays can be configured out
			if((!parms.SIGNAL_EXECUTION_SUNDAYS && current_day_sunday) || current_day_saturday)
			{                
                skip_reason = bfromcstr("due to SIGNAL_EXECUTION_SUNDAYS = false");
                skip_signal = true;
			}
            
			// printing skipped signals potentially floods terminal, do so only if verbose_flag is set
			if(skip_signal && verbose_flag)
            {
                skip_signal_print_reason(daily_signals->Sig[current_signal], skip_reason);
                bdestroy(skip_reason);
                continue; // goto next signal
            }
            
            // if verbose_flag is not set but current signal is to be skipped- skip :)
            if(skip_signal && !verbose_flag)
            {
                bdestroy(skip_reason);
                continue;
            }

			float theMarketWeight = 0;
            theMarketWeight = market_list->getMarketWeight(market_list, marketPos);
            
            // execute signals only if current market weights allow that (e.g. weight != 0 but don't use exactly 0 because weight is a float)
            if(theMarketWeight > 0.01)
            {
                if(biseqcstr(parms.PORTFOLIO_ALLOCATION, "kelly"))
                {
                    // check if the signal direction is in line with Kelly criteria 
                    // e.g. long signal: kelly leverage > 0; short signal: kelly leverage < 0 
                    if((daily_signals->Sig[current_signal]->type == longsignal) &&
                        market_list->getKellyLeverage(market_list, marketPos) < 0)
                    {
                        skip_reason = bfromcstr("due to Kelly leverage suggesting to short market");
                        skip_signal = true;
                    }
                    else if((daily_signals->Sig[current_signal]->type == shortsignal) &&
                        market_list->getKellyLeverage(market_list, marketPos) > 0)
                    {
                        skip_signal = true;
                        skip_reason=bfromcstr("due to Kelly leverage suggesting to long market");
                    }
                 }          
            }
            else
            {
                skip_reason = bfromcstr("due to current market weight 0");
                skip_signal = true;
            }
            
            // printing skipped signals potentially floods terminal, do so only if verbose_flag is set
            if(skip_signal && verbose_flag)
            {
                skip_signal_print_reason(daily_signals->Sig[current_signal], skip_reason);
                bdestroy(skip_reason);
                continue; // goto next signal
            }
            // if verbose_flag is not set but current signal is to be skipped- skip :)
            if(skip_signal && !verbose_flag)
            {
                bdestroy(skip_reason);
                continue;                
            }
   
            float quantity = 0;
            float stoploss = new_stop_loss(market_list->markets[marketPos], market_daynr_idx, daily_signals->Sig[current_signal]->type);

            bool cap_flag = false;  // will be set to true if position size has to be capped
            
            // for Stocks: calculate resulting quantity
            if(biseqcstr(*market_list->markets[marketPos]->markettype, "STOCK"))
            {
                    float current_cash = the_account->getCash(the_account); 
                    float portfolio_sum = current_cash + the_account->getRiskFreeEquity(the_account);
                    float buyquote_market = market_list->markets[marketPos]->Ind[market_closePos]->QUOTEVEC[market_daynr_idx];
                    float risk_market = 0;     // the risk of one unit in market currency
                    switch(daily_signals->Sig[current_signal]->type)
                    {
                        case longsignal: 
                            risk_market = buyquote_market - stoploss;
                            break;
                        case shortsignal: 
                            risk_market = stoploss - buyquote_market;
                            break;
                        case undefined:
                            break;
                    }
                    // translate risk of one unit into account currency
                    float buyquote_account = market_list->markets[marketPos]->getTranslationCurrencyQuotebyIndex(market_list->markets[marketPos], market_daynr_idx);
                    float risk_account = risk_market / buyquote_account;
                    buyquote_account = buyquote_account * buyquote_market;
                    
                    // put it all together  and round up/down
                    quantity = (portfolio_sum * parms.RISK_PER_POSITION) / risk_account;
                    quantity = round(quantity);
                    
                    // cap quantity to POS_SIZE_CAP in case of very tight stop losses the quantity might be way too high,
                    // resulting in undesirable high allocation of capital to single position
                    float max_pos_size = parms.POS_SIZE_CAP * portfolio_sum;
                    if((quantity * buyquote_account) > max_pos_size)
                    {
                        quantity = round(max_pos_size / buyquote_account);
                        cap_flag = true;
                    }
                    
                    if(((quantity * buyquote_account) > current_cash) || quantity == 0)
                    {
                        skip_reason = bfromcstr("due to insufficient cash");
                        skip_signal = true;
                    }
                
                    // printing skipped signals potentially floods terminal, do so only if verbose_flag is set
                    if(skip_signal && verbose_flag)
                    {
                        skip_signal_print_reason(daily_signals->Sig[current_signal], skip_reason);
                        bdestroy(skip_reason);
                        continue; // goto next signal
                    }
                    // if verbose_flag is not set but current signal is to be skipped- skip :)
                    if(skip_signal && !verbose_flag)
                    {
                        bdestroy(skip_reason);
                        continue; 
                    }
            }
            else 
                quantity = theMarketWeight;
         
            the_portfolio->addNewElement(the_portfolio, 
                market_list->markets[marketPos],
                daily_signals->Sig[current_signal]->type, quantity, 
                market_daynr_idx, market_closePos, 
                bdata(*daily_signals->Sig[current_signal]->name), 
                the_account, marketPos, sl_list->nr_records);      
           
            // get idx of last created portfolio_element
            unsigned int element_idx = the_portfolio->getNrElements(the_portfolio) - 1;

            // set Stop Loss
            the_portfolio->elements[element_idx]->stoploss = stoploss;
            
            // create SL record and add latest stoploss
            sl_list->addNewSL(sl_list, bdata(*market_list->markets[marketPos]->symbol), 0,
                              the_portfolio->elements[element_idx]->Buyquote_account->nr_digits, 
                              bdata(the_portfolio->elements[element_idx]->Buyquote_account->datevec[0]), bdata(parms.SL_TYPE));
            
            unsigned int sl_idx = sl_list->nr_records-1;
            sl_list->SL[sl_idx]->QuoteObj->appendQuoteRecords(sl_list->SL[sl_idx]->QuoteObj, 1);
            sl_list->SL[sl_idx]->QuoteObj->setQuoteTupel(sl_list->SL[sl_idx]->QuoteObj,0, sl_list->SL[sl_idx]->buydate, current_day, stoploss);
            
            // calculate fee
            float fee = calculate_fee(the_portfolio->elements[element_idx]);
            the_account->sum_fees = the_account->sum_fees + fee;
            
            // set executed flag to current signal
            daily_signals->Sig[current_signal]->executed = true;
            
            // add orderbook entry
            orderbook->addNewElement(orderbook, the_portfolio->elements[element_idx], buyorder, fee);
            
            // print out message to terminal
            daily_signals->Sig[current_signal]->printTable(daily_signals->Sig[current_signal]);
            
            // update portfolio cash, differentiate between stock-like markets and 
            // future/CFD-like markets (on the latter you are trading on margin thus not
            // reducing your cash when buying
            // TODO: revisit the following equity/P_L calculation, should be done inside portfolio_elements_* now!
            if((biseqcstr(*the_portfolio->elements[element_idx]->markettype, "CFDFUT")) ||
                    (biseqcstr(*the_portfolio->elements[element_idx]->markettype, "CFDCUR")))
            {
                printf(" --> executed! (SL %.*f), Quant: %.2f", the_portfolio->elements[element_idx]->Buyquote_market->nr_digits, 
                                                    the_portfolio->elements[element_idx]->stoploss,
                                                    theMarketWeight);
            }
            
            else if(biseqcstr(*the_portfolio->elements[element_idx]->markettype, "STOCK"))                
            {
                // substract invested money from cash
                float old_cash = the_account->getCash(the_account);
                    
                the_account->setCash(the_account, old_cash - the_portfolio->elements[element_idx]->invested_value - fee);
    
                printf(" --> executed! (quant. %.2f, SL %.*f) invested: %.*f, new cash %*f %s", 
                                   the_portfolio->elements[element_idx]->quantity,
                                    the_portfolio->elements[element_idx]->Buyquote_market->nr_digits, 
                                    the_portfolio->elements[element_idx]->stoploss, the_account->nr_digits, 
                                    the_portfolio->elements[element_idx]-> invested_value, 
                                    the_account->nr_digits, the_account->getCash(the_account), bdata(*the_account->currency));
                if(verbose_flag)
                    printf(" (fee: %.2f)", fee);
                if(cap_flag & verbose_flag)
                    printf(" (position capped)");
            }
            // reset flags for next signal
            cap_flag = false;

		}	// end of loop through all signals of a given day
		daily_signals->saveToDB(daily_signals);
		daily_signals->destroy(daily_signals);
        
        // update portfolio equity
        the_portfolio->updatePortfolioEquity(the_portfolio);
          
        // update account equity
        the_account->setEquity(the_account, the_portfolio->getPortfolioEquity(the_portfolio));
        the_account->setRiskFreeEquity(the_account, the_portfolio->getPortfolioRiskFreeEquity(the_portfolio));
        
        // update performance performance records
        bstring *current_date = NULL;
        current_date = init_1d_array_bstring(1); 
        current_date = get_bstr_from_daynr(current_day);
        performance_total->QuoteObj->setQuoteTupel(performance_total->QuoteObj, performance_idx, current_date, current_day, the_account->getTotal(the_account));
        performance_cash->QuoteObj->setQuoteTupel(performance_cash->QuoteObj, performance_idx, current_date, current_day, the_account->getCash(the_account));
        performance_equity->QuoteObj->setQuoteTupel(performance_equity->QuoteObj, performance_idx, current_date, current_day,the_account->getEquity(the_account));
        performance_rf_equity->QuoteObj->setQuoteTupel(performance_rf_equity->QuoteObj, performance_idx, current_date, current_day,the_account->getRiskFreeEquity(the_account));

        if(the_account->allTimeHigh < the_account->getTotal(the_account))
        {
            the_account->allTimeHigh = the_account->getTotal(the_account);
        }
        
        performance_total_high->QuoteObj->setQuoteTupel(performance_total_high->QuoteObj, performance_idx, current_date, current_day,the_account->allTimeHigh);
        
        performance_idx = performance_idx + 1;
        free_1d_array_bstring(current_date,1);
	} // end of loop through execution period
    
    // dump al SL data to db
    if(parms.SL_SAVE_DB)
    {
        sl_list->saveToDB(sl_list);
    }
    
	// cleanup
	performance_total->saveToDB(performance_total);
    performance_cash->saveToDB(performance_cash);
    performance_equity->saveToDB(performance_equity);
    performance_rf_equity->saveToDB(performance_rf_equity);
    performance_total_high->saveToDB(performance_total_high);
    
	performance_total->destroy(performance_total);
    performance_cash->destroy(performance_cash);
    performance_equity->destroy(performance_equity);
    performance_rf_equity->destroy(performance_rf_equity);
    performance_total_high->destroy(performance_total_high);
    
	signals_sorted->destroy(signals_sorted);

    sl_list->destroy(sl_list);
    
    return EXIT_SUCCESS;
    
error_quote:
    return EXIT_FAILURE;
    
error_market_daynr_idx:
    // cleanup what is possible
    signals_sorted->destroy(signals_sorted);
    the_account->destroy(the_account);
    market_list->destroy(market_list);
    the_signals->destroy(the_signals);
    the_portfolio->destroy(the_portfolio);
 	performance_total->destroy(performance_total);
    performance_cash->destroy(performance_cash);
    performance_equity->destroy(performance_equity);
    performance_rf_equity->destroy(performance_rf_equity);   
    exit(EXIT_FAILURE);    
    
error_market:
	performance_total->destroy(performance_total);
    performance_cash->destroy(performance_cash);
    performance_equity->destroy(performance_equity);
    performance_rf_equity->destroy(performance_rf_equity);
    exit(EXIT_FAILURE);
}

void skip_signal_print_reason(class_signal *the_signal, bstring the_reason)
{
    the_signal->printTable(the_signal);
    printf(ANSI_COLOR_YELLOW" skipped "ANSI_COLOR_RESET);
    printf("%s", bdata(the_reason));    
}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief calculate transaction fee for portfolio element
 *
 * This function calculates the transaction fee given on settings and the 
 * current portfolio element.
 * @param the_element the portfolio element 
 * @return fee as float
 */
///////////////////////////////////////////////////////////////////////////////
float calculate_fee(class_portfolio_element *the_element)
{
    extern parameter parms; // parms are declared global in main.c
	extern bool verbose_flag; // declared global in main.c
    
    float fee = 0.0;
    
    fee = parms.TRANSACTION_COSTS_FIX + the_element->current_value * parms.TRANSACTION_COSTS_PERCENTAGE;
   
    return fee;
}
// End of file
