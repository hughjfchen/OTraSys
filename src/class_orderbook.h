/* class_orderbook.h
 * declarations for class_orderbook.c, interface for "orderbook" class
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2020 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_orderbook.h
 * @brief Header file for class_orderbook.c, public member declarations
 *
 * This file contains the "public" available data and functions of the 
 * orderbook "class". 
 * Public values/functions are accessible like FooObj->BarMethod(FooObj, [..])
 * @author Denis Zetzmann
 */ 

#ifndef CLASS_ORDERBOOK_H
#define CLASS_ORDERBOOK_H

#include <stdbool.h>

#include "class_account.h"
#include "datatypes.h"
#include "class_portfolio_element.h"
#include "class_order.h"
#include "class_market_list.h"

struct _orderbook_private; 	/**< opaque forward declaration, this structs contains
				non-public/ private data/functions */
				
typedef struct _class_orderbook class_orderbook;

// typedefs function pointers to make declaration of struct better readable
typedef void (*destroyOrderbookFunc)(struct _class_orderbook*);
typedef struct _class_orderbook*(*cloneOrderbookFunc)(const struct _class_orderbook*);
typedef void (*printOrderbookTableFunc)(const struct _class_orderbook*); 
typedef unsigned int (*getNrOrderbookElementsFunc)(struct _class_orderbook*);
typedef void (*addOrderbookElementFunc)(struct _class_orderbook*, const struct _class_order*);
typedef void (*addNewOrderbookElementFunc)(struct _class_orderbook*, const struct _class_portfolio_element*, enum _order_type, float);
typedef unsigned int (*getOrderIndexFunc)(const struct _class_orderbook*, bstring*, bstring* , enum _longshort, enum _order_type,  bstring* );
typedef void (*saveOrderbookDBFunc)(struct _class_orderbook*);
typedef void (*loadOrderbookDBFunc)(struct _class_orderbook*, struct _class_market_list*);

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief	definition of an orderbook and its describing properties
 * 
 * This struct describes the properties that form an orderbook. Mainly, an 
 * orderbook is a list of orders, combined with some additional 
 * information (like nr of elements in orderbook) and methods to deal with the
 * orderbook.
 */
struct _class_orderbook
{
	//public part
	// DATA
	class_order** orders; 		/**< vector to members of orer elements */
	bstring* name;			/**< identifier of portfolio */ 
	
	// METHODS, call like: foo_obj->bar_method(foo_obj)
	destroyOrderbookFunc destroy; /**< "destructor", see class_orderbook_destroyOrderbookImpl() */
	cloneOrderbookFunc clone;	/**< create new object with same data as given one, see class_orderbook_cloneOrderbookImpl() */
	printOrderbookTableFunc printTable;	/**< prints out orderbook in tabular format, see class_orderbook_printOrderbookTableImpl() */
	getNrOrderbookElementsFunc getNrElements;  /**< gets nr of orderbook entries, see  class_orderbook_getNrElementsImpl() */
	addOrderbookElementFunc addElement;	/**< adds existing order to orderbook, see class_orderbook_addElementImpl()  */
	addNewOrderbookElementFunc addNewElement; 	/**< create and add new order to orderbook, see class_orderbook_addNewElementImpl() */
	getOrderIndexFunc getOrderIndex;  /**< return index of specified order in orderlist, see class_orderbook_getOrderIndexImpl() */
	saveOrderbookDBFunc saveToDB; /**< saves orderbook to database, see class_orderbook_saveToDBImpl() */
	loadOrderbookDBFunc loadFromDB; /**< loads orderbook from database, see class_orderbook_loadFromDBImpl() */

	//private part
	struct _orderbook_private *orderbook_private;	/*<< opaque pointer to private data and functions */
	// ... add methods
};

// "constructor" for orderbook "objects"
class_orderbook* class_orderbook_init(char* name);

#endif // CLASS_ORDERBOOK_H

// eof
