/* class_orderbook.c
 * Implements a "class"-like struct in C which handles orderbooks
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2020 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_orderbook.c
 * @brief routines that implement a class-like struct which represents 
 * orderbooks
 *
 * This file contains the definitions of the "class" orderbook
 * 
 * @author Denis Zetzmann
 */ 

#include <stdlib.h>
#include <stdio.h>

#include "arrays.h"
#include "bstrlib.h"
#include "datatypes.h"
#include "debug.h"
#include "class_account.h"
#include "class_order.h"
#include "class_orderbook.h"
#include "class_quote.h"
#include "datatypes.h"
#include "database.h"

static void class_orderbook_setMethods(class_orderbook* self);
static void class_orderbook_destroyOrderbookImpl(class_orderbook* self);
static class_orderbook* class_orderbook_cloneOrderbookImpl(const class_orderbook* original);
static void class_orderbook_printOrderbookTableImpl(const class_orderbook* self); 
static unsigned int class_orderbook_getNrElementsImpl(class_orderbook* self);
static void class_orderbook_addElementImpl(class_orderbook* self, const class_order* the_element);
static void class_orderbook_addNewElementImpl(class_orderbook* self, const class_portfolio_element* portfolio_entry, order_type ordertype, float fee);
static unsigned int class_orderbook_getOrderIndexImpl(const class_orderbook* self, bstring* orderdate, bstring* symbol, signal_type signaltype, order_type ordertype, bstring* signalname);
static void class_orderbook_saveToDBImpl(class_orderbook* self);
static void class_orderbook_loadFromDBImpl(class_orderbook* self, class_market_list* markets);
                   
///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "private" parts of the orderbook class.Do not touch your private parts. 
 * 
 * This struct holds the data that is considered to be private, e.g. cannot be
 * accessed outside this file (to do that use the getter/setter functions).
 * The struct is refered by an opaque pointer of "orderbook" object
 * @see orderbook_class.h definition of orderbook class 
 * 
 */
///////////////////////////////////////////////////////////////////////////////
struct _orderbook_private{
	unsigned int nr_elements;	/**< nr of elements in orderbook */
};
typedef struct _orderbook_private orderbook_private;

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "constructor" function for orderbook objects
 *
 * This function initializes memory and maps the functions to the placeholder
 * pointers of the struct methods. 
 * 
 * @param name pointer string with orderbook name
 
 * @return pointer to new object of class order
*/
////////////////////////////////////////////////////////////////////////////////
class_orderbook* class_orderbook_init(char* name)
{
	class_orderbook *self = NULL;
	
	// allocate memory for object
	self = ZCALLOC(1, sizeof(class_orderbook));
	
	//allocate memory for pointer to orders array
	self->orders = (class_order**)ZCALLOC(1,sizeof(class_order));
	
	// reserve memory for private parts
	self->orderbook_private = ZCALLOC(1, sizeof(orderbook_private));
	
	//allocate memory for rest of the object
 	self->name = init_1d_array_bstring(1);

	//init data
 	*self->name = bfromcstr(name);
	self->orderbook_private->nr_elements = 0;
	
 	// set methods
    class_orderbook_setMethods(self);
	
	return self;	
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief internal function that sets methods for orderbook objects
 *
 * all member functions of the class are pointers to functions, that are set 
 * by this routine.
 * 
 * @param self pointer to orderbook struct
*/
////////////////////////////////////////////////////////////////////////////////
static void class_orderbook_setMethods(class_orderbook* self)
{
 	self->destroy = class_orderbook_destroyOrderbookImpl;
 	self->clone = class_orderbook_cloneOrderbookImpl;
 	self->printTable = class_orderbook_printOrderbookTableImpl;
	self->getNrElements = class_orderbook_getNrElementsImpl;
	self->addElement = class_orderbook_addElementImpl;
	self->addNewElement = class_orderbook_addNewElementImpl;
	self->getOrderIndex = class_orderbook_getOrderIndexImpl;    
    self->saveToDB = class_orderbook_saveToDBImpl;
    self->loadFromDB = class_orderbook_loadFromDBImpl;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief Destroys an orderbook object
 *
 * This function destroys an orderbook struct by freeing the occupied 
 * memory
 * 
 * @param self pointer to orderbook struct
 */
///////////////////////////////////////////////////////////////////////////////
static void class_orderbook_destroyOrderbookImpl(class_orderbook* self)
{
	//destroy all included orders
	for(unsigned int i=0; i<self->orderbook_private->nr_elements; i++)
	{
		self->orders[i]->destroy(self->orders[i]);
	}
	free(self->orders);
	
	// free private data
	free(self->orderbook_private);
	
	// free public data
	free_1d_array_bstring(self->name,1);
  
	free(self); 
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief clone an orderbook object
 *
 * clone complete orderbook object (deep copy original into clone), if 
 * original object gets modified or destroy()ed, the clone will still live
 * @param original orderbook pointer to original 
 * @returns orderbook pointer to object that will hold copy of original
 */
///////////////////////////////////////////////////////////////////////////////
static class_orderbook* class_orderbook_cloneOrderbookImpl(const class_orderbook* original)
{
	class_orderbook* newclone = NULL;
	
	// allocate memory for object
	newclone = ZCALLOC(1, sizeof(class_orderbook));
	
	//allocate memory for pointer to orders array
	newclone->orders = (class_order**)ZCALLOC(1,sizeof(class_order));
	
	// reserve memory for private parts
	newclone->orderbook_private = ZCALLOC(1, sizeof(orderbook_private));

	//allocate memory for rest of the object
 	newclone->name = init_1d_array_bstring(1);
	
 	// set methods
    class_orderbook_setMethods(newclone);

	//copy data
 	*newclone->name = bstrcpy(*original->name);
	newclone->orderbook_private->nr_elements = 0;
	for(unsigned int i=0; i<original->orderbook_private->nr_elements; i++)
	{
		newclone->addElement(newclone, original->orders[i]);
	}
	
	return newclone;
}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief prints orderbook info
 *
 * This function prints orderbook info to terminal
 * 
 * @param *self pointer to orderbook struct
 */
///////////////////////////////////////////////////////////////////////////////
static void class_orderbook_printOrderbookTableImpl(const class_orderbook* self)
{
	printf("\n%s, %u elements", bdata(*self->name), self->orderbook_private->nr_elements);
	for(unsigned int i=0; i<self->orderbook_private->nr_elements; i++)
		self->orders[i]->printTable(self->orders[i]);
    printf("\n");
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief adds existing order object to orderbook
 * 
 * Add an existing order object to the given orderbook
 * 
 * @param self pointer to orderbook object
 * @param the_element pointer to order object
*/
////////////////////////////////////////////////////////////////////////////
static void class_orderbook_addElementImpl(class_orderbook* self, const class_order* the_element)
{
	// increase nr of elements in portfolio object
	self->orderbook_private->nr_elements = self->orderbook_private->nr_elements + 1;
	
	// realloc indicator vector for new element object
	self->orders = (class_order**)realloc(self->orders, (self->orderbook_private->nr_elements + 1) * sizeof(class_order));
    if(self->orders == NULL)
    {
        log_err("Memory allocation failed");
        exit(EXIT_FAILURE);
    }
    
	// clone given element within portfolio object
	self->orders[self->orderbook_private->nr_elements - 1] = the_element->clone(the_element);
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief adds new order object to orderbook
 * 
 * Add a new order object to the given orderbook
 * 
 * @param self pointer to orderbook object
 * @param portfolio_entry pointer to portfolio_element object
 * @param ordertype order_type enum (buyorder/sellorder)
*/
////////////////////////////////////////////////////////////////////////////
static void class_orderbook_addNewElementImpl(class_orderbook* self, const class_portfolio_element* portfolio_entry, order_type ordertype, float fee)
{
	// increase nr of elements in portfolio object
	self->orderbook_private->nr_elements = self->orderbook_private->nr_elements + 1;
	
	// realloc indicator vector for new element object
	self->orders = (class_order**)realloc(self->orders, (self->orderbook_private->nr_elements + 1) * sizeof(class_order));
    if(self->orders == NULL)
    {
        log_err("Memory allocation failed");
        exit(EXIT_FAILURE);
    }
    
	// create temporary order object from portfolio portfolio_entry
	class_order* tmp_order = NULL;
	tmp_order = class_order_init(portfolio_entry, ordertype, fee);
	
	// clone given element within portfolio object
	self->orders[self->orderbook_private->nr_elements - 1] = tmp_order->clone(tmp_order);
	
	// destroy temporary order
	tmp_order->destroy(tmp_order);	
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief return the nr of orderbook entries
 * 
 * This method returns the nr of entries within orerbook object
 * 
 * @param self pointer to orderbook object
 * @returns nr of elements as uint
*/
////////////////////////////////////////////////////////////////////////////
static unsigned int class_orderbook_getNrElementsImpl(class_orderbook* self)
{
	return self->orderbook_private->nr_elements;
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief return index of a specific order in orderbook
 * 
 * Checks if an order with the given characteristics is present and returns
 * it´s index. Exits with an error otherwise
 * 
 * @param self pointer to orderbook object
 * @param orderdate bstring ptr with date
 * @param symbol bstring ptr with market symbol
 * @param signaltype longsignal/shortsignal enum
 * @param ordertype buyorder/sellorder enum
 * @param signalname bstring ptr with the name of the trigger signal
 * @returns uint with position within class_orderbook->orders
*/
////////////////////////////////////////////////////////////////////////////
static unsigned int class_orderbook_getOrderIndexImpl(const class_orderbook* self, bstring* orderdate, bstring* symbol, signal_type signaltype, order_type ordertype,  bstring* signalname)
{
	unsigned int position = 0;
	
	// loop through all elements of orderbook
	for(position = 0; position < self->orderbook_private->nr_elements; position++)
	{	
		// shortcut: check if long/short and buy/sell is identical for current index
 		if((signaltype != self->orders[position]->signaltype) || (ordertype != self->orders[position]->ordertype))
 			continue;		// no, next

 		bstring the_date = NULL;
		
		switch(ordertype)		// copy the relevant date to compare with
		{
			case buyorder:
				the_date = bstrcpy(*self->orders[position]->buydate);
				break;
			case sellorder:
				the_date = bstrcpy(*self->orders[position]->selldate);
				break;
			default: 	// should not happen
				the_date = bfromcstr("1900-01-01");
				break;
		}
		
		
		if(biseq(*orderdate, the_date) && biseq(*symbol, *self->orders[position]->symbol) && biseq(*signalname, *self->orders[position]->signalname))
		{
			bdestroy(the_date);
			return position;
		}
		bdestroy(the_date);
	}
	
	// if we didn't exit the loop above, we did not find the order
	// display error to terminal and exit	
	bstring signaltypestring = NULL;
	bstring ordertypestring = NULL;
	switch(signaltype)
	{
		case longsignal:
			signaltypestring = bfromcstr("Long signal");
			break;
		case shortsignal:
			signaltypestring = bfromcstr("Short signal");
			break;
		default: 
			signaltypestring = bfromcstr("undefined");
			break;
	}
	switch(ordertype)
	{
		case buyorder:
			ordertypestring = bfromcstr("Buy order");
			break;
		case sellorder:
			ordertypestring = bfromcstr("Sell order");
			break;
		default:
			ordertypestring = bfromcstr("[Undefined ordertype]");
			break;
	}
	log_err("\n%s with date %s, symbol %s (%s, signalname %s) found within orderbook %s\n", 
		bdata(ordertypestring), bdata(*orderdate), bdata(*symbol), bdata(signaltypestring), bdata(*signalname), bdata(*self->name));

	exit(EXIT_FAILURE);
}

static void class_orderbook_saveToDBImpl(class_orderbook* self)
{
    db_mysql_update_orderbook(self);
}

static void class_orderbook_loadFromDBImpl(class_orderbook* self, class_market_list* markets)
{
    db_mysql_get_orderbook(self, markets);
}

// eof
