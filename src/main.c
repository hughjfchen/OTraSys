/* main.c
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2020 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file main.c
 * @brief main program file of OTraSys- the trading system framework
 *
 * This is the main file of OTraSys- the trading system framework, which
 * contains main(). It reads the command line options, establishes the 
 * connection to the mysql database, updates the quotes and indicators and
 * calls the routines that check for buy signals and execute them.
 * @author Denis Zetzmann
 */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <time.h>
#include <stdbool.h>
#include </usr/include/mysql/mysql.h>

#ifdef DEBUG
    #define _GNU_SOURCE
    #include <fenv.h>
#endif
    
#include "debug.h"
#include "constants.h"
#include "bstrlib.h"
#include "datatypes.h"
#include "database.h"
#include "date.h"
#include "readconf.h"
#include "constants.h"
#include "arrays.h"
#include "execution.h"
#include "statistics.h"
#include "utilities.h"
#include "ichimoku.h"
#include "indicators_general.h"
#include "indicators_ichimoku.h"

#include "class_account.h"
#include "class_market.h"
#include "class_market_list.h"
#include "class_portfolio.h"
#include "class_signal_list.h"
#include "class_order.h"
#include "class_orderbook.h"
#include "class_stoploss_record.h"
#include "class_stoploss_list.h"

const int MAJOR_VERSION_NR=0;
const int RELEASE_NR = 5;
const int SUB_RELEASE_NR = 1;
const char SUBRELEASE_SUFFIX[]="";

/**< Flag set by ‘--verbose’ */
bool verbose_flag=false;

parameter parms;   /**< create a global parameter struct that holds config */
MYSQL *quote_db_handle = NULL;      /**< global variable, used widely in database.c */
MYSQL *system_db_handle = NULL;     /**< global variable, used widely in database.c */

int feenableexcept(int, int, int, int); // forward declaration to prevent compiler warning

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief main function that is called when starting the program
 *
 * This function reads the command line options, establishes the 
 * connection to the mysql database, updates the quotes and indicators and
 * calls the routines that check for buy signals and execute them.
 * 
 * @param argc int with number of command line arguments
 * @param argv pointer to [argc x 1] array of command line arguments
 * @return 0 when successfull, 1 otherwise
 */
///////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
    
#ifdef DEBUG
    feenableexcept(FE_DIVBYZERO, FE_INVALID, FE_OVERFLOW, FE_UNDERFLOW);
#endif    

    quote_db_handle = mysql_init(quote_db_handle);
    system_db_handle = mysql_init(system_db_handle);
    
    int opt = 0;
	
	// Specify possible command line options
	static struct option long_options[] = 
	{
		{"setup_db", no_argument, 0, 's' },
		{"configfile", no_argument, 0, 'c'},
		{"version", no_argument, 0, 'V' },
		{"help", no_argument, 0, '?' },
		{"verbose", no_argument, 0, 'v' },
		{"reports", no_argument, 0, 'r'},
		{"portfolio", no_argument, 0, 'p'},
        {"orderbook", no_argument, 0, 'o'},
        {"account", no_argument, 0, 'a'},
        {"set-cash", required_argument, 0, 'C'},
		{0, 0, 0, 0}
	};	
	
	int long_index =0;

	// global variables for skipping evaluation/execution part
	bool reports_only = false;	// go straight to statistics if started with --reports or -r
	bool portfolio_only = false;	// just print out portfolio 
	bool orderbook_only = false;   // just print out orderbook
	bool account_only = false;     // just prinnt out account info
	bool setCash = false;          // set cash from command line

	bstring configfile_name = bfromcstr("./config/otrasys.conf");
	extern int optind, opterr, optopt;
	extern char *optarg;
	
    float newCash = 0;

	// get all command line options, loop through them and parse them
	// (short version only as long version is mapped by getopt_long
	while((opt = getopt_long(argc, argv,"sc:V?vrpoaC:", long_options, &long_index)) != -1)
	{
		switch (opt) 
		{
			case 's':	// --setup_db
				// init and set default/fallback config parameters
				init_parameters(&parms);
				/* read config file */	
				log_info("Reading config file");
				if(!(parse_config(configfile_name, &parms)==0))
				{
					log_err("Could not parse config file, exiting");
					exit(EXIT_FAILURE);
				}
				
				fprintf(stdout, "Be aware that this will re-create database %s on %s\n", bdata(parms.DB_SYSTEM_NAME), bdata(parms.DB_SYSTEM_HOSTNAME));
				fprintf(stdout, "Existing data in this database WILL BE LOST! press <CTRL> + <c> to abort now...\n");
				fprintf(stdout, "Please enter password of MySQL root user: ");
				char root_pw[64];
				// ask for mysql root user password
				if (fgets(root_pw, sizeof root_pw, stdin)) 
				{
					root_pw[strcspn(root_pw, "\n")] = 0;  // trim \n from input
					// call function that creates db and tables
					mysql_create_db(
						bdata(parms.DB_SYSTEM_HOSTNAME),
						"root",
						root_pw,
						bdata(parms.DB_SYSTEM_NAME), 
						parms.DB_SYSTEM_PORT, 
						bdata(parms.DB_SYSTEM_SOCKET), 
						parms.DB_SYSTEM_FLAGS);
				}
				db_close_mysql_connection(system_db_handle);                
				return 0;
				break;
			case 'c':	// --configfile
				bassigncstr(configfile_name, "./config/");
				bconcat(configfile_name, bfromcstr(optarg));
				fprintf(stdout, "\nUsing config file %s\n", bdata(configfile_name));
				break;
			case 'V':	// --version
				fprintf(stdout, "\nOTraSys the Open Trading System Framework %i.%i.%i%s\n", MAJOR_VERSION_NR, RELEASE_NR,SUB_RELEASE_NR,SUBRELEASE_SUFFIX);
                fprintf(stdout, "An open source framework to create trading systems\n");
				fprintf(stdout, "\nplease visit https://spare-time-trading.de\n");
                fprintf(stdout, "\nCopyright (C) 2016-2020 Denis Zetzmann, d1z@gmx.de");
                fprintf(stdout, "\nLicense GPLv3+: GNU GPL Version 3 or later <https://https://gnu.org/licenses/gpl.html");
                fprintf(stdout, "\nThis is free software: your are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by the law.\n");
				return 0;
				break;
			case 'v':	// --verbose
				verbose_flag = true;
				fprintf(stdout, "\nOTraSys v%i.%i.%i%s", MAJOR_VERSION_NR, RELEASE_NR,SUB_RELEASE_NR,SUBRELEASE_SUFFIX);
				fprintf(stdout, "\nVerbose flag (-v, --verbose) set)\n");
				break;
			case '?':  // --help
				fprintf(stdout, "\nOTraSys- the open Trading System Framework");
				fprintf(stdout, " v%i.%i.%i%s", MAJOR_VERSION_NR, RELEASE_NR,SUB_RELEASE_NR,SUBRELEASE_SUFFIX);
                fprintf(stdout, "\nAn open source framework to create trading systems");
                fprintf(stdout, "\n\nUsage:");
				fprintf(stdout, "\notrasys [OPTION]");
				fprintf(stdout, "\n\nOPTIONS:");
				fprintf(stdout, "\n  -?, --help \t\t\tThis help page");
                fprintf(stdout, "\n  -V, --version\t\t\tshows version number");
                fprintf(stdout, "\n  -v, --verbose\t\t\tbe verbose (flood terminal with status messages)\n");
				fprintf(stdout, "\n  -c, --configfile\t\tselect configfile within /config (default is /config/otrasys.conf)");
				fprintf(stdout, "\n  -s, --setup_db\t\tsetup necessary database (db is cleared if exists), root password needed!");
				fprintf(stdout, "\n\t\t\t\t(Note that this option is exclusive to the other command line options)\n");
				fprintf(stdout, "\n  -r  --reports\t\t\tskip signal evaluation/execution, do statistics based on orderbook in database");
				fprintf(stdout, "\n  -p  --portfolio\t\tskip signal evaluation/execution, print out current portfolio and account info from database");
                fprintf(stdout, "\n  -o  --orderbook\t\tskip signal evaluation/execution, print out current orderbook and account info from database");
                fprintf(stdout, "\n  -a  --account\t\t\tskip signal evaluation/execution, print out current account info");
                fprintf(stdout, "\n  -C  --set-cash AMOUNT\t\tskip signal evaluation/execution, set cash for current account to AMOUNT");
				fprintf(stdout, "\n\n\nplease send bug reports to d1z@gmx.de");
				fprintf(stdout, "\nplease visit https://spare-time-trading.de\n");
                fprintf(stdout, "\nCopyright (C) 2016-2020 Denis Zetzmann, d1z@gmx.de");
                fprintf(stdout, "\nLicense GPLv3+: GNU GPL Version 3 or later <https://https://gnu.org/licenses/gpl.html");
                fprintf(stdout, "\nThis is free software: your are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by the law.\n");
                bdestroy(configfile_name);
				return 0;
				break;
			case 'r':	// --reports
				fprintf(stdout, "\nSkipping signal generation/trade execution");
				fprintf(stdout, "\nevaluating orderbook in database\n");
				reports_only = true;
				break;
			case 'p':	// --portfolio
				fprintf(stdout, "\nSkipping signal generation/trade execution");
				fprintf(stdout, "\nDisplaying current portfolio\n");
				portfolio_only = true;
				break;
            case 'o':   // --orderbook
                fprintf(stdout, "\nSkipping signal generation/trade execution");
                fprintf(stdout, "\nCurrent orderbook in database\n");
                orderbook_only = true;
                break;
            case 'a':   // --account
                fprintf(stdout, "\nSkipping signal generation/trade execution");
                fprintf(stdout, "\nCurrent account information\n");
                account_only = true;
                break;
            case 'C':   // --set-cash
                if(is_float(optarg, &newCash))
                {
                    setCash = true;
                }
                else
                {
                    printf("  -C/--set-cash requires a float number as argument!\n");
                    exit(EXIT_FAILURE);
                }                
                break;
			default:	// catcher for any fallthrough (aka programmer forgot a 'break')
				break;
		}
	}
	
	clock_t program_begin, program_end;
	double running_time;
	
	program_begin = clock();	// start clock
	
	// get todays date
	bstring* today;
	today= get_today_bstr();

	/* set default/fallback config parameters */
	init_parameters(&parms);

	/* read config file */	
	log_info_nonewline("Reading config file %s...", bdata(configfile_name));
	if(!(parse_config(configfile_name, &parms)==0))
	{	
		log_err("Could not parse config file, terminating!");
		exit(EXIT_FAILURE);
	}
    printf(" done!\n"); // reading config file
    
	// read and parse symbol file, get symbols and info like ticks, the value
	// of a single pip, the currency of the value and the type of symbol
    log_info_nonewline("Parsing market info in %s..." , bdata(parms.CONFIG_SYMBOLFILE));
    
	bstring *symbols=NULL;		// array with different symbols	
	bstring *identifiers=NULL;		// array with symbol´s identifiers (e.g. ISIN/WKN/...
	bstring *csvfiles = NULL;      // array with csv filenames
	unsigned int *tradeable=NULL;	// arrays with info if market is tradeable
	float *ticks_pips = NULL;			
	float *contract_sizes = NULL; 
    float *weights = NULL;
	bstring *currencies = NULL;	// currency of each symbol
	bstring *symboltypes = NULL;	// type (CFDFUT, CFDCUR) of symbol
	unsigned int *currency_table_position=NULL;	// pointer to position in currency conversion table
	
	// initialize all arrays with at least 1 entry, will be re-allocated
	// in parse_symbolfile() if needed
	symbols = init_1d_array_bstring(1);	
	identifiers = init_1d_array_bstring(1);
    csvfiles = init_1d_array_bstring(1);
	tradeable = init_1d_array_uint(1);
	ticks_pips = init_1d_array_float(1);
	contract_sizes = init_1d_array_float(1);
    weights = init_1d_array_float(1);
	currencies = init_1d_array_bstring(1);
	symboltypes = init_1d_array_bstring(1);
	currency_table_position = init_1d_array_uint(1);	//re-allocated in create_currency_translation_table
	
	// now parse the symbol configuration file (parms.CONFIG_SYMBOLFILE)
	// note parse_... also resizes symbols[] and identifiers[] if needed
	unsigned int nr_symbols=0;	// total number of symbols in config file
	nr_symbols = parse_symbolfile(parms.CONFIG_SYMBOLFILE, &symbols, 
			&identifiers, &csvfiles, &tradeable, &ticks_pips, &contract_sizes, &weights, 
            &currencies, &symboltypes);
    printf(" done!\n");
 
	// from now on all symbol names are stores in symbols[], the filenames in identifiers[]
	// and the number of symbols in nr_symbols
	
	//* connect to mysql Server */
	if(!biseqcstr(parms.TERMINAL_EVAL_RESULT,"html"))
		log_info_nonewline("Connecting to databases %s and %s...", bdata(parms.DB_QUOTES_NAME), bdata(parms.DB_SYSTEM_NAME));
    
    // connect first to quote repository, then to system database
    connect_mysql_database(
        quote_db_handle, 
        bdata(parms.DB_QUOTES_HOSTNAME),
		bdata(parms.DB_QUOTES_USERNAME),
		bdata(parms.DB_QUOTES_PASSWORD),
		bdata(parms.DB_QUOTES_NAME), 
		parms.DB_QUOTES_PORT, 
		bdata(parms.DB_QUOTES_SOCKET), 
		parms.DB_QUOTES_FLAGS);
    
	connect_mysql_database(
		system_db_handle,
        bdata(parms.DB_SYSTEM_HOSTNAME),
		bdata(parms.DB_SYSTEM_USERNAME),
		bdata(parms.DB_SYSTEM_PASSWORD),
		bdata(parms.DB_SYSTEM_NAME), 
		parms.DB_SYSTEM_PORT, 
		bdata(parms.DB_SYSTEM_SOCKET), 
		parms.DB_SYSTEM_FLAGS);
    printf(" done!\n"); // connecting to database
    
 	// set up account "object", load data from db
 	class_accounts *account= class_accounts_init(parms.ACCOUNT_CURRENCY, parms.ACCOUNT_CURRENCY_DIGITS);
 	account->loadFromDB(account);
	
 	// create objects that will hold all information
    class_market_list* the_markets = class_market_list_init("the markets");
    class_signal_list* the_signals = class_signal_list_init("All ichimoku Signals");
	class_portfolio* the_portfolio = class_portfolio_init("the portfolio");
	class_orderbook* the_orderbook = class_orderbook_init("the orderbook");
    
    // skip evaluation/execution part if started with display only command line options
	if((!reports_only) && (!portfolio_only) && (!account_only) && (!orderbook_only) && (!setCash))	
	{		
		if(!biseqcstr(parms.TERMINAL_EVAL_RESULT,"html"))
			log_info_nonewline("Setting up markets, indicators and calculating signals...");
		
		// loop to update quotes, indicators and signals
		for(unsigned int current_symbol=0; current_symbol < nr_symbols; current_symbol++)
		{
			// add market objects to market_list
            the_markets->addNewElement(the_markets, bdata(symbols[current_symbol]), bdata(identifiers[current_symbol]), 
						get_nr_of_digits(ticks_pips[current_symbol]), 
						tradeable[current_symbol],
						contract_sizes[current_symbol],
                        weights[current_symbol],
						bdata(currencies[current_symbol]), 
						bdata(symboltypes[current_symbol]),
						bdata(identifiers[current_symbol]),
						"lalala",
						"lululu");
            
			// load quotes from .csv files in /data into database
            // NOTE: mysql_updatequotes is deprecated and will be removed
            // the update of the quote database is done externally now
// 			mysql_updatequotes(symbols[current_symbol], csvfiles[current_symbol]);
			
			// load quotes from database, init indicator position´s shortcut
			update_indicators(the_markets->markets[current_symbol]);
			the_markets->markets[current_symbol]->initIndicatorPositions(the_markets->markets[current_symbol]);
			
			// check tradeable flag, if market is configured to not be tradeable -> skip it
			if(!tradeable[current_symbol])
				continue;
			
			// (re-)calculate indicators with latest data	
			ichimoku_update_indicators(the_markets->markets[current_symbol]);
			the_markets->markets[current_symbol]->initIndicatorPositions(the_markets->markets[current_symbol]);
            
			// evaluate signals based on indicators
  			ichimoku_evaluate_signals(the_markets->markets[current_symbol], the_signals);
            
            // save indicators in DB NOTE: this is completely optional, from now on the system will use generated signals
            the_markets->saveAllIndicatorsDB(the_markets);
		}

        // save signals
        the_signals->saveToDB(the_signals);   // remember that signals in db with "executed"-flag won't be overwritten
        the_signals->destroy(the_signals);
        
        // load signals from database, including the ones that have the "executed"-flag set
        class_signal_list* loaded_signals = NULL;
        loaded_signals = class_signal_list_init("signals loaded from database");
        loaded_signals->loadFromDB(loaded_signals, "ichimoku_daily_signals");     

        // filter out signals that are not configured to be executed
        for(unsigned int i = 0; i< loaded_signals->nr_signals; i++)
            {
            if(!execute_specific_signal(*loaded_signals->Sig[i]->name))
            {
                loaded_signals->removeSignal(loaded_signals, i);
                i--;
            }
        }
        
        printf(" %i markets loaded, %i signals waiting\n", the_markets->getNrElements(the_markets), loaded_signals->nr_signals);
		
        // create portfolio and load existing portfolio from database
        log_info_nonewline("Loading portfolio from database... ");
        the_portfolio->loadFromDB(the_portfolio);
        printf("Loaded \"%s\" with currently %i entries.\n", bdata(*the_portfolio->name), the_portfolio->getNrElements(the_portfolio));

        // load existing orderbook from database
        log_info_nonewline("Loading orderbook from database... ");
        the_orderbook->loadFromDB(the_orderbook, the_markets);
        printf("Loaded \"%s\" with currently %i entries.\n", bdata(*the_orderbook->name), the_orderbook->getNrElements(the_orderbook));

        // all markets are loaded, including their indicators, look for and assign the 
        // translation currencies (depends on "close" indicator present)
        if(!biseqcstr(parms.TERMINAL_EVAL_RESULT,"html"))
            log_info_nonewline("Preparing execution...");
        for(unsigned int current_symbol=0; current_symbol < nr_symbols; current_symbol++)
        {
            // check for and assign a translation currency (market to account currency) for each market
            the_markets->markets[current_symbol]->assignTranslationCurrency(the_markets->markets, nr_symbols, the_markets->markets[current_symbol]);
        }
 		
        // now that each market has its translation currency, 
        // get rid of non tradeable markets
        the_markets->removeNonTradeable(the_markets);

        // sort markets by lowest daynr SIGNAL_DAYS_TO_EXECUTION days ago
        class_market_list* markets_sorted = NULL;
        markets_sorted = the_markets->getSortedListbyDayNr(the_markets, parms.INDI_DAYS_TO_UPDATE - parms.SIGNAL_DAYS_TO_EXECUTE);
        the_portfolio->setMarketListPositions(the_portfolio, markets_sorted); // assign markets already in portfolio (from db) the right positions in market_list
        printf(" all set to go!\n"); // preparing execution

        // execute signals
        if(!biseqcstr(parms.TERMINAL_EVAL_RESULT,"html"))
            log_info_nonewline("Executing signals...\n");
        fflush(stdout);
        execution_manager(account, markets_sorted, loaded_signals, the_portfolio, the_orderbook);
        fflush(stdout);
        printf(" ... all done!\n");
        
        // ===================================================================================
        fflush(stdout);
        
//         printf("\n\n=============================\ntest stuff :)\n=============================\n");
// 
//         printf("\n");
        // =================================== end of test stuff (: ================   
        // ===================================================================================
        fflush(stdout);
        
        markets_sorted->destroy(markets_sorted);
        loaded_signals->destroy(loaded_signals);
        
       	program_end = clock();	// stop clock
        running_time = (double) (program_end-program_begin) *1000 / CLOCKS_PER_SEC;
	
        if(!biseqcstr(parms.TERMINAL_EVAL_RESULT,"html"))
            log_info_nonewline("Total time: %f ms\n", running_time);
	
        // update portfolio equity
        the_portfolio->updatePortfolioEquity(the_portfolio);
        // make sure account equity is up to date
        account->setEquity(account, the_portfolio->getPortfolioEquity(the_portfolio));
        account->setRiskFreeEquity(account, the_portfolio->getPortfolioRiskFreeEquity(the_portfolio));
    
        log_info_nonewline("Update database...");
        // update database before quitting
        account->saveToDB(account);
        the_portfolio->saveToDB(the_portfolio);
        the_orderbook->saveToDB(the_orderbook);
	
        /* now remove all lines that contain NULL for tenkan
        * this is needed because the future dates for senkou are just
        * guesses, leaving out weekends (but having no clue for 
        * holidays). So with time there would be gaps in the database
        * where Senkou values exist (they were projected into future) 
        * but no quotes or other indicators like tenkan.
        * Note that this function removes the future dates also, so
        * it must be called before updating senkouA or B */
        db_mysql_remove_null_rows();
	
        // save indicators 
        the_markets->saveAllIndicatorsDB(the_markets);
    }  
    
    // now do the display-only specific stuff
    // if configured to do so, do the statistic evaluation of the orderbook
    if(biseqcstr(parms.TERMINAL_STATISTICS, "text") || reports_only)
    {
        account->loadFromDB(account);
        statistics_manager(account);
    }    
    if (portfolio_only)
    { 
        // create portfolio and load existing portfolio from database
        log_info_nonewline("Loading portfolio from database... ");
        the_portfolio->loadFromDB(the_portfolio);
        printf("Loaded \"%s\" with currently %i entries.\n", bdata(*the_portfolio->name), the_portfolio->getNrElements(the_portfolio));
        the_portfolio->printTable(the_portfolio);
        account->printTable(account);
        printf("\n");
    }
    if (account_only)
    {
        account->printTable(account);
        printf("\n");
    }
    if(setCash)
    {
        printf("\nUpdating account data!\nOld Account Cash:");
        account->printTable(account);
        account->setCash(account, newCash);
        printf("\nNew Account Cash:");
        account->printTable(account);
        account->saveToDB(account);
    }
    if (orderbook_only)
    {
        log_info_nonewline("Adding markets...");
       	// set up markets, as their info is needed for orderbook entries
		for(unsigned int current_symbol=0; current_symbol < nr_symbols; current_symbol++)
		{
			// add market objects to market_list
            the_markets->addNewElement(the_markets, bdata(symbols[current_symbol]), 
                        bdata(identifiers[current_symbol]),
						get_nr_of_digits(ticks_pips[current_symbol]), 
						tradeable[current_symbol],
						contract_sizes[current_symbol],
                        weights[current_symbol],
						bdata(currencies[current_symbol]), 
						bdata(symboltypes[current_symbol]),
						bdata(identifiers[current_symbol]),
						"lalala",
						"lululu");
        }
        
        // load existing orderbook from database
        log_info_nonewline("Loading orderbook from database... ");
        the_orderbook->loadFromDB(the_orderbook, the_markets);
        printf("Loaded \"%s\" with currently %i entries.\n", bdata(*the_orderbook->name), the_orderbook->getNrElements(the_orderbook));
        the_orderbook->printTable(the_orderbook);
        account->printTable(account);
        printf("\n");
    }            

	db_close_mysql_connection(system_db_handle);	// don't do anything in mysql after that- and YES, that´s a lesson learned :)
	db_close_mysql_connection(quote_db_handle);
    
    printf("done!\n");

    log_info_nonewline("Shutting down... ");
	//cleanup
    the_markets->destroy(the_markets);
	account->destroy(account);
	the_portfolio->destroy(the_portfolio);
    the_orderbook->destroy(the_orderbook);
	bdestroy(configfile_name);
    
    destroy_parameters(&parms);
	
	free_1d_array_bstring(symbols, nr_symbols);
	free_1d_array_bstring(identifiers, nr_symbols);
    free_1d_array_bstring(csvfiles, nr_symbols);
	free_1d_array_uint(tradeable);
	free_1d_array_float(ticks_pips);
	free_1d_array_float(contract_sizes);
    free_1d_array_float(weights);
	free_1d_array_bstring(currencies, nr_symbols);
	free_1d_array_bstring(symboltypes, nr_symbols);
	free_1d_array_uint(currency_table_position);
	free_1d_array_bstring(today,1);
	
	printf("done!\n");
    
	
// 	return EXIT_SUCCESS;
}

/* End of file */
