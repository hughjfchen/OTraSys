/* class_signals.c
 * Implements a "class"-like struct in C which handles signals
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2020 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_signals.c
 * @brief routines to handle signal data structure
 *
 * This file contains the functions which dealt with creating, destroying and
 * copying signal structs
 */

#include <stdlib.h>

#include "arrays.h"
#include "debug.h"
#include "class_signals.h"
#include "database.h"

static void class_signal_setMethods(class_signal* self);
static void class_signal_destroyImpl(class_signal *self);
static class_signal* class_signal_cloneImpl(const class_signal* original);
static void class_signal_printImpl(const class_signal *self);
static void class_signal_saveSignalImpl(const class_signal *self);

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief Creates a signal object
 *
 * This function creates a signal struct, allocates memory and initializes some
 * of its components
 * 
 * @param datestring char pointer to date as "YYYY-MM-DD"
 * @param daynr uint with nr of days since 1900-01-01
 * @param price float with price at which signal occured
 * @param indicator_quote float with quote of indicator when signal occured
 * @param symbol char pointer with underlying market 
 * @param type signal_type (longsignal/shortsignal)
 * @param strength strength_type (weak/neutral/strong)
 * @param nrOfDigits unint with significant nr of digits of underlying market
 * @param signal_name char pointer with name of the new signal
 * @param signal_description char ptr with short description of the signal
 * @param tablename char ptr with name of the mysql db table, which stores this
 *        specific signal
 * @param amp_info char ptr with amplifiying info for signal
 * @return pointer to new signal object
 */
///////////////////////////////////////////////////////////////////////////////
class_signal *class_signal_init(char* datestring, unsigned int daynr, float price, 
                      float indicator_quote, char* symbol, char* identifier, 
                      signal_type type, strength_type strength, 
                      unsigned int nrOfDigits, char* signal_name, 
                      char* signal_description, char* amp_info, char* tablename)
{
	class_signal *self = NULL;
	// allocate memory for object
	self = ZCALLOC(1, sizeof(class_signal));
	
	// call base class constructor
	self->QuoteObj = class_quotes_init(symbol, identifier, 1, nrOfDigits, "signal", "none");
	// as for signals QuoteObj contains only 1st element, skip ...XYZvec[0]
	*self->QuoteObj->datevec = bfromcstr(datestring);	
	*self->QuoteObj->daynrvec = daynr;
	*self->QuoteObj->quotevec = price;
	
	self->name = init_1d_array_bstring(1);
	self->description = init_1d_array_bstring(1);
	self->amp_info = init_1d_array_bstring(1);
	self->db_tablename = init_1d_array_bstring(1);

	// init data
	self->type = type;
	self->strength = strength;
	self->indicator_quote = indicator_quote;
	*self->name = bfromcstr(signal_name);
	*self->description = bfromcstr(signal_description);
	*self->amp_info = bfromcstr(amp_info);
	*self->db_tablename = bfromcstr(tablename);
	self->executed = false;
	
	// set methods
    class_signal_setMethods(self);
	
	return self;
}	

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief internal function that sets methods for signal objects
 *
 * all member functions of the class are pointers to functions, that are set 
 * by this routine.
 * 
 * @param self pointer to signal struct
*/
////////////////////////////////////////////////////////////////////////////////
static void class_signal_setMethods(class_signal* self)
{
	self->destroy = class_signal_destroyImpl;
	self->printTable = class_signal_printImpl;
	self->clone = class_signal_cloneImpl;
	self->saveToDB = class_signal_saveSignalImpl;    
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief Destroys a signal object
 *
 * This function destroys a signal struct by freeing the occupied memory
 * 
 * @param self pointer to signal struct
 */
///////////////////////////////////////////////////////////////////////////////
static void class_signal_destroyImpl(class_signal* self)
{
	self->QuoteObj->destroy(self->QuoteObj);
	free_1d_array_bstring(self->name, 1);
	free_1d_array_bstring(self->description, 1);
	free_1d_array_bstring(self->amp_info, 1);
	free_1d_array_bstring(self-> db_tablename,1);
	// free(self->priv);
	free(self); 
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief clone a signal object
 *
 * clone complete signal object (deep copy original into clone), if original 
 * object gets modified or destroy()ed, the clone will still live
 * @param original signal pointer to original signal
 * @returns signal pointer to object that will hold copy of original
 */
///////////////////////////////////////////////////////////////////////////////
class_signal* class_signal_cloneImpl(const class_signal* original)
{
	class_signal* clone = NULL;
	clone = class_signal_init(bdata(*original->QuoteObj->datevec), 
	                    *original->QuoteObj->daynrvec, 
	                    *original->QuoteObj->quotevec, 
	                    original->indicator_quote, 
						bdata(*original->QuoteObj->symbol), 
                        bdata(*original->QuoteObj->identifier), 
                        original->type, 
	                    original->strength, original->QuoteObj->nr_digits, 
	                    bdata(*original->name), bdata(*original->description),
	                    bdata(*original->amp_info), 
	                    bdata(*original->db_tablename));
	
	clone->executed = original->executed;
	
	return clone;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief prints signal info
 *
 * This function prints signal info to terminal
 * 
 * @param self pointer to signal struct
 */
///////////////////////////////////////////////////////////////////////////////
static void class_signal_printImpl(const class_signal* self)
{
	bstring signaltype = NULL;
	if(self->type == longsignal)
		signaltype = bfromcstr("long");
	else if (self->type == shortsignal)
		signaltype = bfromcstr("short");
	
	bstring signalstrength = NULL;
	if(self->strength == weak)
		signalstrength = bfromcstr("weak");
	else if(self->strength == neutral)
		signalstrength = bfromcstr("neutral");
	else if(self->strength == strong)
		signalstrength = bfromcstr("strong");
	printf("\n[signal] %s %s (%s), %s %s %s %s @ %.*f", 
			bdata(*self->QuoteObj->datevec),  bdata(*self->QuoteObj->symbol), 
            bdata(*self->QuoteObj->identifier),
	   		bdata(signalstrength), bdata(signaltype), bdata(*self->name), 
	   		bdata(*self->amp_info), self->QuoteObj->nr_digits, 
	   		*self->QuoteObj->quotevec);
	bdestroy(signaltype);
	bdestroy(signalstrength);
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief saves signal to databse
 *
 * This function saves the signal to the database. Tablename is stored within
 * signals´s data (->db_tablename)
 * 
 * @param self pointer to signal struct
 */
///////////////////////////////////////////////////////////////////////////////
static void class_signal_saveSignalImpl(const class_signal* self)
{
	db_mysql_update_signal(*self->THE_SYMBOL, *self->QuoteObj->identifier, *self->db_tablename, 
				*self->SIG_DATE, *self->SIG_PRICE, 
				self->indicator_quote, self->type, self->strength,
				self->SIG_DIGITS, *self->name, *self->description, 
				*self->amp_info, self->executed);
}

// End of file
