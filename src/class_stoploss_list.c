 /* class_stoploss_list.c
 * Implements a "class"-like struct in C which handles SL lists
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2020 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_stoploss_list.c
 * @brief routines to handle stoploss_list data structure
 *
 * This file contains the functions which dealt with creating, destroying and
 * copying stoploss_list structs
 */

#include <stdlib.h>
#include <stdio.h>


#include "debug.h"
#include "database.h" 
#include "class_stoploss_list.h"
#include "class_stoploss_record.h"
#include "arrays.h"
#include "bstrlib.h"

static void class_stoploss_list_setMethods(class_stoploss_list* self);
static void class_stoploss_list_destroyImpl(class_stoploss_list *self);
static void class_stoploss_list_addNewSLImpl(class_stoploss_list *self, char* quote_symbol, unsigned int nr_records, unsigned int nrOfDigits, char* buydate, char* stoploss_type);
static void class_stoploss_list_PrintTableImpl(const class_stoploss_list* self);
static void class_stoploss_list_addSLImpl(class_stoploss_list *self, const class_sl_record *the_sl);
static void class_stoploss_list_saveToDBImpl(const class_stoploss_list* self);

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief Creates a stoploss_list object
 *
 * This function creates a stoploss_list struct, allocates memory and initializes 
 * some of its components
 * 
 * @param listname char pointer with name of this list
 * @return pointer to new stoploss_list object
 */
///////////////////////////////////////////////////////////////////////////////
class_stoploss_list *class_stoploss_list_init(char* listname)
{
 	class_stoploss_list* self = NULL;

 	// allocate memory 
 	self = ZCALLOC(1, sizeof(class_stoploss_list));
 
 	// do not allocate memory for vector of inherited objects (there are none at this point)
    self->SL = NULL;

 	// allocate memory for rest of components
 	self->name = init_1d_array_bstring(1);
 	
 	// init data;
 	*self->name = bfromcstr(listname);
 	self->nr_records = 0;	
	
	// set methods
    class_stoploss_list_setMethods(self);
    
	return self;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief internal function that sets methods for stoploss_list objects
 *
 * all member functions of the class are pointers to functions, that are set 
 * by this routine.
 * 
 * @param self pointer to stoploss_list struct
*/
////////////////////////////////////////////////////////////////////////////////
static void class_stoploss_list_setMethods(class_stoploss_list* self)
{
    self->destroy = class_stoploss_list_destroyImpl;
    self->printTable = class_stoploss_list_PrintTableImpl;
    self->addNewSL = class_stoploss_list_addNewSLImpl;
    self->addSL = class_stoploss_list_addSLImpl;
    self->saveToDB = class_stoploss_list_saveToDBImpl;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief Destroys a stoploss_list object
 *
 * This function destroys a stoploss_list struct by freeing the occupied memory
 * 
 * @param self pointer to stoploss_list struct
 */
///////////////////////////////////////////////////////////////////////////////
static void class_stoploss_list_destroyImpl(class_stoploss_list *self)
{
	for(unsigned int i=0; i<self->nr_records; i++)
	{
		self->SL[i]->destroy(self->SL[i]);
	}
	free(self->SL);

	// free public data
	free_1d_array_bstring(self->name,1);

	free(self);   
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief adds a new sl_record object to stoploss_list object
 *
 * This function creates a new sl_record object which is then added to given 
 * stoploss_list object
 * 
 * @param self pointer to stoploss_list struct
 * @param quote_symbol  string with symbol name
 * @param nr_records uint number of records for this symbol
 * @param nrOfDigits uint with significant nr of digits for sl quote
 * @param *buydate bstring ptr with date when position was bought
 * @param *stoploss_type bstring ptr 
 */
///////////////////////////////////////////////////////////////////////////////
static void class_stoploss_list_addNewSLImpl(class_stoploss_list *self, 
                                char* quote_symbol, unsigned int nr_records, 
                                unsigned int nrOfDigits, char* buydate, 
                                char* stoploss_type)
{
	// increase nr of signals in signal_list object
	self->nr_records = self->nr_records + 1;
	
	// realloc SL vector for new signal object
	self->SL = (class_sl_record**) realloc(self->SL, (self->nr_records) * sizeof(class_sl_record));

    if(self->SL == NULL)
    {
        log_err("Memory allocation failed");
        exit(EXIT_FAILURE);
    }
    
	// now create and init new sl_record object in SL vector
	self->SL[self->nr_records - 1] = class_sl_record_init(quote_symbol, nr_records,
                                        nrOfDigits, buydate, stoploss_type);
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief adds an existing stoploss_record object to stoploss_list object
 *
 * This function adds an existing sl_record object to given stoploss_list object
 * 
 * @param self pointer to stoploss_list struct
 * @param the_sl ptr to sl_record object
*/
///////////////////////////////////////////////////////////////////////////////
// static void class_signalList_addSignalImpl(class_signal_list* self, class_signal* theSignal)
static void class_stoploss_list_addSLImpl(class_stoploss_list *self, const class_sl_record *the_sl)
{
	// increase nr of signals in signal_list object
	self->nr_records = self->nr_records + 1;
	
	// realloc signal vector for new signal object
	self->SL = (class_sl_record**)realloc(self->SL, (self->nr_records) * sizeof(class_sl_record));

    if(self->SL == NULL)
    {
        log_err("Memory allocation failed");
        exit(EXIT_FAILURE);
    }
    
	// now create and init new signal object in signal vector	
    self->SL[self->nr_records - 1] = class_sl_record_init(bdata(*the_sl->QuoteObj->symbol), the_sl->QuoteObj->nr_elements, 
                                            the_sl->QuoteObj->nr_digits, bdata(*the_sl->buydate), bdata(*the_sl->stoploss_type));
    // copy the quotes of the_sl into the list
    self->SL[self->nr_records - 1]->QuoteObj = the_sl->QuoteObj->clone(the_sl->QuoteObj);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief prints all signals in sl_list object to terminal
 * 
 * prints prints info about all sl_records in sl_list object to terminal
 *
 * @param self pointer to stoploss_list object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_stoploss_list_PrintTableImpl(const class_stoploss_list* self)
{
	printf("\nStoploss List \"%s\", containing %i sl records",bdata(*self->name), self->nr_records);
	for(unsigned int i=0; i< self->nr_records; i++)
    {
        unsigned int last_element = self->SL[i]->QuoteObj->nr_elements - 1;
        printf("\n%s\tbuydate: %s\tselldate: %s\ttype:%s", bdata(*self->SL[i]->QuoteObj->symbol),bdata(*self->SL[i]->buydate), bdata(*self->SL[i]->selldate), bdata(*self->SL[i]->stoploss_type));
        printf("\n\tlast SL: %s\t%.*f", bdata(self->SL[i]->QuoteObj->datevec[last_element]), self->SL[i]->QuoteObj->nr_digits, self->SL[i]->QuoteObj->quotevec[last_element]);
    }
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief saves all sl_records within list to database
 * 
 * saves all sl_records within list to database
 * 
 * @param self pointer to stoploss_list object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_stoploss_list_saveToDBImpl(const class_stoploss_list* self)
{
	for(unsigned int i=0; i<self->nr_records; i++)
		self->SL[i]->saveToDB(self->SL[i]);
}

// eof
