/* class_signal_list.c
 * Implements a "class"-like struct in C which handles signal lists
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2020 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_signal_list.c
 * @brief routines to handle signal_list data structure
 *
 * This file contains the functions which dealt with creating, destroying and
 * copying signal_list structs
 */

#include <stdlib.h>

#include "arrays.h"
#include "debug.h"
#include "class_signal_list.h"
#include "database.h" 

static void class_signalList_setMethods(class_signal_list* self);
static void class_signalList_destroyImpl(class_signal_list *self);
static class_signal_list* class_signalList_cloneImpl(const class_signal_list *self);
static void class_signalList_printTableImpl(const class_signal_list* self);
static void class_signalList_addNewSignalImpl(class_signal_list* self, char* datestring, unsigned int daynr, float price, float indicator_quote, char* symbol, char* identifier, signal_type type, strength_type strength, unsigned int nrOfDigits, char* signal_name, char* signal_description, char* amp_info, char* tablename);
static void class_signalList_addSignalImpl(class_signal_list* self, class_signal* theSignal);
static void class_signalList_removeSignalImpl(class_signal_list* self, unsigned int elementToRemove);
static void class_signalList_saveAllSignalsImpl(const class_signal_list* self);
static void class_signalList_loadAllSignalsImpl(class_signal_list* self, char* tablename);
static unsigned int class_signalList_getNrofSignalsPerDaynrImpl(const class_signal_list* self, unsigned int daynr);
static void class_signalList_filterSignalsPerDaynrImpl(class_signal_list* origin, class_signal_list* dest, unsigned int daynr);
static void class_signalList_filterOutSignalsBeforeDaynrImpl(class_signal_list* origin, class_signal_list* dest, unsigned int daynr);
static void class_signalList_filterSignalsPerSymbolImpl(class_signal_list* origin, class_signal_list* dest, bstring symbol);
static void class_signalList_filterSignalsPerStrengthImpl(class_signal_list* origin, class_signal_list* dest, strength_type strength);
static void class_signalList_filterOutSignalsPerStrengthImpl(class_signal_list* origin, class_signal_list* dest, strength_type strength);
static void class_signalList_filterSignalsPerTrendtypeImpl(class_signal_list* origin, class_signal_list* dest, signal_type type);
static void class_signalList_filterOutMultipleSignalsPerDayImpl(class_signal_list* origin, class_signal_list* dest);
static class_signal_list* class_signalList_getSortedListByDaynrImpl(class_signal_list* self);

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief Creates a signal_list object
 *
 * This function creates a signal_list struct, allocates memory and initializes 
 * some of its components
 * 
 * @param listname char pointer with name of this list
 * @return pointer to new signal_list object
 */
///////////////////////////////////////////////////////////////////////////////
class_signal_list *class_signal_list_init(char* listname)
{
	class_signal_list* self = NULL;
	
	// allocate memory 
	self = ZCALLOC(1, sizeof(class_signal_list));

	// bugfix: do not allocate memory for vector of inherited objects (there are none at this point)
// 	self->Sig = (class_signal**)ZCALLOC(1,sizeof(class_signal));
    self->Sig = NULL;
	
	// allocate memory for rest of components
	self->name = init_1d_array_bstring(1);
	
	// init data;
	*self->name = bfromcstr(listname);
	self->nr_signals = 0;	
	
	// set methods
    class_signalList_setMethods(self);
    
	return self;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief internal function that sets methods for signal_list objects
 *
 * all member functions of the class are pointers to functions, that are set 
 * by this routine.
 * 
 * @param self pointer to signal_list struct
*/
////////////////////////////////////////////////////////////////////////////////
static void class_signalList_setMethods(class_signal_list* self)
{
	self->destroy = class_signalList_destroyImpl;
	self->clone = class_signalList_cloneImpl;
	self->printTable = class_signalList_printTableImpl;	
	self->addNewSignal = class_signalList_addNewSignalImpl;
	self->addSignal = class_signalList_addSignalImpl;
    self->removeSignal = class_signalList_removeSignalImpl;
	self->saveToDB = class_signalList_saveAllSignalsImpl;
    self->loadFromDB = class_signalList_loadAllSignalsImpl;
	self->getNrofSignalsPerDaynr = class_signalList_getNrofSignalsPerDaynrImpl;
	self->filterSignalsPerDaynr = class_signalList_filterSignalsPerDaynrImpl;
	self->filterOutSignalsBeforeDaynr = class_signalList_filterOutSignalsBeforeDaynrImpl;
	self->filterSignalsPerSymbol = class_signalList_filterSignalsPerSymbolImpl;
	self->filterSignalsPerStrength = class_signalList_filterSignalsPerStrengthImpl;
	self->filterOutSignalsPerStrength = class_signalList_filterOutSignalsPerStrengthImpl;
	self->filterSignalsPerTrendtype = class_signalList_filterSignalsPerTrendtypeImpl;
	self->filterOutMultipleSignalsADay = class_signalList_filterOutMultipleSignalsPerDayImpl;
    self->getSortedListByDaynr = class_signalList_getSortedListByDaynrImpl;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief Destroys a signal_list object
 *
 * This function destroys a signal_list struct by freeing the occupied memory
 * 
 * @param self pointer to signal_list struct
 */
///////////////////////////////////////////////////////////////////////////////
static void class_signalList_destroyImpl(class_signal_list *self)
{
	for(unsigned int i=0; i<self->nr_signals; i++)
	{
		self->Sig[i]->destroy(self->Sig[i]);
	}
	free(self->Sig);

	// free public data
	free_1d_array_bstring(self->name,1);

	free(self);   
}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief clones a signal_list object
 *
 * This function clones the given signal_list object, creating a deep copy
 * 
 * @param self pointer to signal_list struct
 * @return pointer to signal_list clone
 */
///////////////////////////////////////////////////////////////////////////////
static class_signal_list* class_signalList_cloneImpl(const class_signal_list *self)
{
	class_signal_list* clone = NULL;
	clone = class_signal_list_init(bdata(*self->name));
	for(unsigned int i = 0; i< self->nr_signals; i++)
    {
        class_signal* tmp_sig = NULL;
        tmp_sig = self->Sig[i]->clone(self->Sig[i]);
        clone->addSignal(clone, tmp_sig);
        tmp_sig->destroy(tmp_sig);
        
// 		clone->addSignal(clone, self->Sig[i]->clone(self->Sig[i]));
    }
	
	return clone;	
}


///////////////////////////////////////////////////////////////////////////////
/**
 * @brief adds a new signal object to signal_list object
 *
 * This function creates a new signal object which is then added to given 
 * signal_list object
 * 
 * @param self pointer to signal_list struct
 * @param datestring char pointer to date as "YYYY-MM-DD"
 * @param daynr uint with nr of days since 1900-01-01
 * @param price float with price at which signal occured
 * @param indicator_quote float with quote of indicator when signal occured
 * @param symbol char pointer with underlying market 
 * @param identifier char ptr with identifier (e.g. ISIN) of market
 * @param type signal_type (longsignal/shortsignal)
 * @param strength strength_type (weak/neutral/strong)
 * @param nrOfDigits unint with significant nr of digits of underlying market
 * @param signal_name char pointer with name of the new signal
 * @param signal_description char ptr with short description of the signal
 * @param tablename char ptr with name of the mysql db table, which stores this
 *        specific signal
 * @param amp_info char ptr with amplifiying info for signal
 */
///////////////////////////////////////////////////////////////////////////////
static void class_signalList_addNewSignalImpl(class_signal_list* self, char* datestring, 
				unsigned int daynr, float price, float indicator_quote, 
                char* symbol, char* identifier, signal_type type, strength_type strength, 
                unsigned int nrOfDigits, char* signal_name, 
                char* signal_description, char* amp_info, char* tablename)
{
	// increase nr of signals in signal_list object
	self->nr_signals = self->nr_signals + 1;
	
	// realloc signal vector for new signal object
	self->Sig = (class_signal**) realloc(self->Sig, (self->nr_signals) * sizeof(class_signal));
    if(self->Sig == NULL)
    {
        log_err("Memory allocation failed");
        exit(EXIT_FAILURE);
    } 
    
	// now create and init new signal object in signal vector
	self->Sig[self->nr_signals - 1] = class_signal_init(datestring, daynr, price,
	                                              indicator_quote, symbol, identifier, type,
	                                              strength, nrOfDigits, 
	                                              signal_name, 
	                                              signal_description, amp_info,
	                                              tablename);
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief adds an existing signal object to signal_list object
 *
 * This function adds an existing signal object to given signal_list object
 * 
 * @param self pointer to signal_list struct
 * @param the_signal signal pointer to signal object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_signalList_addSignalImpl(class_signal_list* self, class_signal* theSignal)
{
	// increase nr of signals in signal_list object
	self->nr_signals = self->nr_signals + 1;
	
	// realloc signal vector for new signal object
	self->Sig = (class_signal**)realloc(self->Sig, (self->nr_signals) * sizeof(class_signal));
    if(self->Sig == NULL)
    {
        log_err("Memory allocation failed");
        exit(EXIT_FAILURE);
    } 
    
	// now create and init new signal object in signal vector	
	self->Sig[self->nr_signals - 1] = class_signal_init(bdata(*theSignal->SIG_DATE), *theSignal->SIG_DAYNR, 
							*theSignal->SIG_PRICE, theSignal->indicator_quote, 
							bdata(*theSignal->QuoteObj->symbol), bdata(*theSignal->QuoteObj->identifier), theSignal->type, 
							theSignal->strength, theSignal->SIG_DIGITS, 
							bdata(*theSignal->name), bdata(*theSignal->description),
							bdata(*theSignal->amp_info), bdata(*theSignal->db_tablename));
	// copy executed status flag
	self->Sig[self->nr_signals - 1]->executed = theSignal->executed;

}



///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief removes an existing signal from signal_list
 * 
 * This method removes an existing signal from a list, elements
 * after the removed one move up by 1 index nr (leaving no holes)
 * 
 * @param self pointer to signal_list object
 * @param elementToRemove uint with index of signal within list to remove
*/
////////////////////////////////////////////////////////////////////////////
static void class_signalList_removeSignalImpl(class_signal_list* self, unsigned int elementToRemove)
{
	// check if elementToRemove really is within array limits
	check(elementToRemove <= (self->nr_signals -1), "Trying to remove element with index nr %u failed, \nsignal_list \"%s\" has only %u elements (starting with index 0)!", errorlabel,
	     elementToRemove,  bdata(*self->name), self->nr_signals);
	
	// create a temporary vector of signals
	class_signal **tempElements = NULL;
	
	// alloc memory for 1 element less than original list
	tempElements = (class_signal**)ZCALLOC((self->nr_signals-1), sizeof(class_signal));
	
	// copy everything before the elementToRemove
	if(elementToRemove != 0)
	{
		for(unsigned int i=0; i<elementToRemove; i++)
			tempElements[i] = self->Sig[i]->clone(self->Sig[i]);
	}
	
	// copy everything after the elementToRemove
	if(elementToRemove != (self->nr_signals - 1))
	{
		for(unsigned int i=elementToRemove + 1; (i<self->nr_signals); i++)
			tempElements[i-1] = self->Sig[i]->clone(self->Sig[i]);
	}
	
	//destroy all originally included portfolio_element objects
	for(unsigned int i=0; i<self->nr_signals; i++)
	{
		self->Sig[i]->destroy(self->Sig[i]);
	}
	free(self->Sig);
	
	// redirect pointer to portfolio_elements to freshly copied list
	self->Sig = tempElements;
	
	// decrease element counter
	self->nr_signals = self->nr_signals - 1;
	
	return;

errorlabel:
	self->destroy(self);
	exit(EXIT_FAILURE);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief prints all signals in signal_list object to terminal
 * 
 * prints prints all signals in signal_list object to terminal, example:
 * 
 * [signal] 2017-08-08 EURJPY, weak short Kijun Cross Death X @ 129.650
 *
 * @param self pointer to signal_list object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_signalList_printTableImpl(const class_signal_list* self)
{
	printf("\nSignal List \"%s\", containing %i signals",bdata(*self->name), self->nr_signals);
	for(unsigned int i=0; i< self->nr_signals; i++)
    {
		self->Sig[i]->printTable(self->Sig[i]);
//         printf("  exec flag: %i", self->Sig[i]->executed);
    }
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief saves all signals within list to database
 * 
 * saves all signals within list to database
 * 
 * @param self pointer to signal_list object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_signalList_saveAllSignalsImpl(const class_signal_list* self)
{
	for(unsigned int i=0; i<self->nr_signals; i++)
		self->Sig[i]->saveToDB(self->Sig[i]);
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief loads all signals within database into given signal list
 * 
 * loads all signals within database into given signal list
 * 
 * @param self pointer to signal_list object
 * @param tablename char ptr with name of the database table that holds the 
 *          signals to be loaded
*/
///////////////////////////////////////////////////////////////////////////////
static void class_signalList_loadAllSignalsImpl(class_signal_list* self, char* tablename)
{
    bstring* tablename_bstr = init_1d_array_bstring(1);
    *tablename_bstr = bfromcstr(tablename);
    db_mysql_get_signals(self, tablename_bstr);
    free_1d_array_bstring(tablename_bstr,1);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief returns nr of signals on a specific daynr
 * 
 * returns nr of signals on a specific daynr
 * 
 * @param self pointer to signal_list object
 * @param daynr uint with the daynr to look at
 * @returns uint with nr of signals on that daynr
*/
///////////////////////////////////////////////////////////////////////////////
static unsigned int class_signalList_getNrofSignalsPerDaynrImpl(const class_signal_list* self, unsigned int daynr)
{
	unsigned int nrSignals = 0;
	for(unsigned int i=0; i<self->nr_signals; i++)
	{
		if(*self->Sig[i]->SIG_DAYNR == daynr)
			nrSignals = nrSignals + 1;
	}
	return nrSignals;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief stores signals on specific daynr from signal_list origin in dest
 * 
 * This function checks all signals within signal_list origin that occured on 
 * a specific daynr, and stores them within signal_list dest
 * 
 * @param origin pointer to signal_list object with all signals
 * @param dest pointer to signal_list that will hold signals on specific daynr
 * @param daynr uint with the daynr to look at
*/
///////////////////////////////////////////////////////////////////////////////
static void class_signalList_filterSignalsPerDaynrImpl(class_signal_list* origin, class_signal_list* dest, unsigned int daynr)
{
	for(unsigned int i=0; i<origin->nr_signals; i++)
	{
		if(*origin->Sig[i]->SIG_DAYNR == daynr)
			dest->addSignal(dest, origin->Sig[i]);
	}
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief filters signals before a specific daynr
 *  
 * This function checks all signals within signal_list origin,
 * and copies only the ones that occured after a given daynr (including the
 * daynr)
 * 
 * @NOTE: This might get very time consuming for large lists
 * 
 * @param origin pointer to signal_list object with all signals
 * @param dest pointer to signal_list that will single signals within a market for a day
 * @param daynr uint with the specific daynr
*/
///////////////////////////////////////////////////////////////////////////////
static void class_signalList_filterOutSignalsBeforeDaynrImpl(class_signal_list* origin, class_signal_list* dest, unsigned int daynr)
{
	for(unsigned int i=0; i<origin->nr_signals; i++)
	{
		if(*origin->Sig[i]->SIG_DAYNR >= daynr)
			dest->addSignal(dest, origin->Sig[i]);
	}
}


///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief stores signals of specific symbol from signal_list origin in dest
 * 
 * This function checks all signals within signal_list origin that occured in
 * a specific market, and stores them within signal_list dest
 * 
 * @param origin pointer to signal_list object with all signals
 * @param dest pointer to signal_list that will hold signals on specific daynr
 * @param symbol bstring with the market to look for
*/
///////////////////////////////////////////////////////////////////////////////
static void class_signalList_filterSignalsPerSymbolImpl(class_signal_list* origin, class_signal_list* dest, bstring symbol)
{
	for(unsigned int i=0; i<origin->nr_signals; i++)
	{
		if(biseq(*origin->Sig[i]->THE_SYMBOL, symbol))
			dest->addSignal(dest, origin->Sig[i]);
	}	
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief stores signals of specific strength (weak/neutral/strong) from 
 * signal_list origin in dest
 * 
 * This function checks all signals within signal_list origin that have
 * a specific strength, and stores them within signal_list dest
 * 
 * @param origin pointer to signal_list object with all signals
 * @param dest pointer to signal_list that will hold signals on specific strength
 * @param strength strength_type (weak/neutral/strong) to look for
*/
///////////////////////////////////////////////////////////////////////////////
static void class_signalList_filterSignalsPerStrengthImpl(class_signal_list* origin, class_signal_list* dest, strength_type strength)
{
	for(unsigned int i=0; i<origin->nr_signals; i++)
	{
		if(origin->Sig[i]->strength == strength)
			dest->addSignal(dest, origin->Sig[i]);
	}
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief filters out specific strength signals from origin when creating
 * signal_list dest
 * 
 * This function checks all signals within signal_list origin that have
 * a specific strength, and copies alls other strength signals to dest 
 * (exluding the specific strength)
 * 
 * @param origin pointer to signal_list object with all signals
 * @param dest pointer to signal_list that will hold signals without specific strength
 * @param strength strength_type (weak/neutral/strong) to look for
*/
///////////////////////////////////////////////////////////////////////////////
static void class_signalList_filterOutSignalsPerStrengthImpl(class_signal_list* origin, class_signal_list* dest, strength_type strength)
{
	for(unsigned int i=0; i<origin->nr_signals; i++)
	{
		if(origin->Sig[i]->strength != strength)
			dest->addSignal(dest, origin->Sig[i]);
	}	
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief stores signals of specific direction (long/short) from 
 * signal_list origin in dest
 * 
 * This function checks all signals within signal_list origin that have
 * a specific direction (long/short), and stores them within signal_list dest
 * 
 * @param origin pointer to signal_list object with all signals
 * @param dest pointer to signal_list that will hold signals on specific daynr
 * @param type trend_type (longsignal/shortsignal) to look for
*/
///////////////////////////////////////////////////////////////////////////////
static void class_signalList_filterSignalsPerTrendtypeImpl(class_signal_list* origin, class_signal_list* dest, signal_type type)
{
	for(unsigned int i=0; i<origin->nr_signals; i++)
	{
		if(origin->Sig[i]->type == type)
			dest->addSignal(dest, origin->Sig[i]);
	}
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief filters out double or more signals that were triggered the same day
 *  
 * This function checks all signals within signal_list origin,
 * and copies only the first signal of a market on each day. 
 * 
 * @NOTE: This might get very time consuming for large lists
 * 
 * @param origin pointer to signal_list object with all signals
 * @param dest pointer to signal_list that will single signals within a market for a day
*/
///////////////////////////////////////////////////////////////////////////////
static void class_signalList_filterOutMultipleSignalsPerDayImpl(class_signal_list* origin, class_signal_list* dest)
{
	dest->addSignal(dest, origin->Sig[0]);
	
	bool add_flag= true;
	
	for(unsigned int i=1; i<origin->nr_signals; i++)
	{
		add_flag = true;
		for(unsigned int j = 0; j<dest->nr_signals; j++)
		{
			if( biseq(*origin->Sig[i]->THE_SYMBOL, *dest->Sig[j]->THE_SYMBOL) && (*origin->Sig[i]->SIG_DAYNR == *dest->Sig[j]->SIG_DAYNR))
			{
				add_flag = false;
				break;
			}
		}
		if(add_flag)
		{
			dest->addSignal(dest, origin->Sig[i]);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief sorts signals of signal_list according to their daynrs
 * 
 * This function takes a list of signals, sorts them according to their
 * daynrs (the date the signal was triggered) and returns a sorted list
 * 
 * @TODO: currently a naive bubblesort is implemented, I couldn't make
 * quicksort work properly (the code is still there, but commented out
 * As this can get time critical for thousands of signals, revisit the
 * quicksort implementation asap!
 * 
 * @param self pointer to signal_list object with all signals
 * @returns pointer so signal_list with sorted signals
*/
///////////////////////////////////////////////////////////////////////////////
static class_signal_list* class_signalList_getSortedListByDaynrImpl(class_signal_list* self)
{
    class_signal_list* list_sorted = NULL;
    list_sorted = self->clone(self);
  
	// shortcut for special condition if nr_signals < 2
	if(list_sorted->nr_signals < 2)
		return list_sorted;
	
	// bubblesort implementation
	// TODO: look again why quicksort (commented out below) didn't work
	for(unsigned int i=1; i<list_sorted->nr_signals; i++)
		for(unsigned int j=0; j< list_sorted->nr_signals - i; j++)
		{
			if(*list_sorted->Sig[j]->DAYNRVEC > *list_sorted->Sig[j+1]->DAYNRVEC)
				{
					class_signal* tmp_sig = NULL;
					tmp_sig = list_sorted->Sig[j]->clone(list_sorted->Sig[j]);
					list_sorted->Sig[j]->destroy(list_sorted->Sig[j]);
					list_sorted->Sig[j] = list_sorted->Sig[j+1]->clone(list_sorted->Sig[j+1]);
					list_sorted->Sig[j+1]->destroy(list_sorted->Sig[j+1]);
					list_sorted->Sig[j+1] = tmp_sig->clone(tmp_sig);
					tmp_sig->destroy(tmp_sig);
				}
		}
 
 	//size_t numbers_len = sizeof(self->Sig)/sizeof(class_signal);
    /* sort array using qsort functions */ 
    //qsort(list_sorted->Sig, numbers_len, sizeof(class_signal), int_cmp);
  
    //#define  MAX_LEVELS  3000

    //int  piv, beg[MAX_LEVELS], end[MAX_LEVELS], i=0, low, high, swap;

    //beg[0]=0; end[0]=list_sorted->nr_signals - 1;
    //while (i>=0) 
    //{
        //low = beg[i]; 
        //high = end[i] - 1;
        //if (low<high) 
        //{
            //class_signal* tmp_signal = NULL;
            //piv=low;
            //tmp_signal = list_sorted->Sig[piv]->clone(list_sorted->Sig[piv]);
            
            //while (low<high) +
            //{
                //while (*list_sorted->Sig[high]->SIG_DAYNR >= *list_sorted->Sig[piv]->SIG_DAYNR && low<high) 
                    //high--; 
                //if (low<high)
                //{
                    //class_signal* tmp_signal2 = NULL;
                    //tmp_signal2 = list_sorted->Sig[high]->clone(list_sorted->Sig[high]);
                    //list_sorted->Sig[low]->destroy(list_sorted->Sig[low]);
                    //list_sorted->Sig[low] = tmp_signal2->clone(tmp_signal2);
                    //low++;
                    //tmp_signal2->destroy(tmp_signal2);
                //}
                //while (*list_sorted->Sig[low]->SIG_DAYNR <= *list_sorted->Sig[piv]->SIG_DAYNR && low<high) 
                    //low++; 
                //if (low<high) 
                //{
                    //class_signal* tmp_signal3 = NULL;
                    //tmp_signal3 = list_sorted->Sig[low]->clone(list_sorted->Sig[low]);
                    //list_sorted->Sig[high]->destroy(list_sorted->Sig[high]);
                    //list_sorted->Sig[high] = tmp_signal3->clone(tmp_signal3);
                    //tmp_signal3->destroy(tmp_signal3);
                    //high--;
                //}
            //}
            //list_sorted->Sig[low]->destroy(list_sorted->Sig[low]);
            //list_sorted->Sig[low] = tmp_signal->clone(tmp_signal);
            //tmp_signal->destroy(tmp_signal);
            
            //beg[i+1]=low+1; 
            //end[i+1]=end[i]; 
            //end[i++]=low;
            //if (end[i]-beg[i]>end[i-1]-beg[i-1]) 
            //{
                //swap=beg[i]; 
                //beg[i]=beg[i-1]; 
                //beg[i-1]=swap;
                //swap=end[i]; 
                //end[i]=end[i-1]; 
                //end[i-1]=swap; 
            //}
        //}
        //else 
        //{
            //i--;
        //}
    //}
    return list_sorted; 
}

// eof
