/* class_account.h
 * declarations for class_account.c
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2020 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_account.h
 * @brief Header file for class_account.c, public member declarations
 *
 * This file contains the "public" available data and functions of the accounts
 * "class". As the public ones are declared here, private ones are declared in
 * account.c´s  accounts_private struct.
 * Public values/functions are accessible like NewObj->setBalance(account, 666)
 * Private values will not be accessible (you have to use the setter/getter
 * functions like setBalance in the example above)

 * @author Denis Zetzmann
 */

#ifndef CLASS_ACCOUNT_H
#define CLASS_ACCOUNT_H

#include<stdbool.h>

#include "bstrlib.h"

// needed to supress compiler in below´s typedefs:
// warning: ‘struct accounts’ declared inside parameter list"
struct _class_accounts;    

struct _accounts_private;  /**< opaque forward declaration, this structs contains
				non-public/ private data/functions */

// typedefs function pointers to make declaration of struct better readable
typedef void (*setCashFunc)(struct _class_accounts *, float newcash);
typedef float (*getCashFunc)(const struct _class_accounts *);
typedef void (*setEquityFunc)(struct _class_accounts *, float newequity);
typedef float (*getEquityFunc)(const struct _class_accounts *);
typedef void (*setRiskFreeEquityFunc)(struct _class_accounts *, float newequity);
typedef float (*getRiskFreeEquityFunc)(const struct _class_accounts *);
typedef float (*getTotalFunc)(const struct _class_accounts *);
typedef void (*setVirginFlagFunc)(struct _class_accounts *, bool virginflag);
typedef bool (*getVirginFlagFunc)(const struct _class_accounts *);
typedef void (*getAccountfromDBFunc) (struct _class_accounts *);
typedef void (*updateAccountDBFunc) (struct _class_accounts *);
typedef void (*printAccountTableFunc) (const struct _class_accounts *);
typedef void (*destroyAccountFunc) (struct _class_accounts *);

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief Declaration of a "class"-like struct which represent accounts
 * 
 * This struct contains the "public" available data and functions of the 
 * accounts "class". As the public ones are declared here, private ones are 
 * declared in account.c´s  accounts_private struct.
 * Public values/functions are accessible like NewObj->setBalance(account, 666)
 * Private values will not be accessible (you have to use the setter/getter
 * functions like setBalance in the example above)
 */
struct _class_accounts {
	// public parts
	bstring* currency; 		/**< account currency */
	unsigned int nr_digits; 		/**< nr of significant digits */
	float sum_fees;        /**< sum of all transaction costs so far */
	float allTimeHigh;     /**< all time high of total (cash + equity */
	
	// Methods
	setCashFunc setCash;	/**<  sets the Cash, see class_accounts_setCashImpl() */
	getCashFunc getCash;	/**< returns Cash, see  class_accounts_getCashImpl() */
	getEquityFunc getEquity;	/**< returns Equity, see  class_accounts_getEquityImpl() */
	setEquityFunc setEquity;	/**< sets Equity, see  class_accounts_setEquityImpl() */
	getRiskFreeEquityFunc getRiskFreeEquity; /**< returns risk free Equity, see  class_accounts_getRiskFreeEquityImpl() */
	setRiskFreeEquityFunc setRiskFreeEquity; /**< sets risk free Equity, see class_accounts_setRiskFreeEquityImpl() */
	getTotalFunc getTotal;     /**< get total (cash + equity), see class_accounts_getTotalImpl()  */
	getVirginFlagFunc getVirginFlag;	/**< returns virgin flag, see class_accounts_getVirginFlagImpl() */
	setVirginFlagFunc setVirginFlag;	/**< sets virgin flag, see class_accounts_setVirginFlagImpl()  */
	getAccountfromDBFunc loadFromDB; /**< load account data from database, see  class_accounts_getAccountfromDB() */
	updateAccountDBFunc saveToDB; /**< update account data to database, see class_accounts_updateAccountDB() */
	printAccountTableFunc printTable; /**< print account info to terminal, see class_accounts_printAccountTable() */
	destroyAccountFunc destroy;	/**< "destructor", see class_accounts_destroyImpl() */
	// private parts
	struct _accounts_private *accounts_priv;	/*<< opaque pointer to private data and functions */
};
typedef struct _class_accounts class_accounts;

class_accounts* class_accounts_init(bstring currency_string, unsigned int digits); 	// "constructor" for accounts objects

#endif // CLASS_ACCOUNT_H
