/* class_portfolio.c
 * Implements a "class"-like struct in C which handles portfolios 
 * 
 * This file is part of OTraSys- The Open Trading System
 * An open source framework to create trading systems
 * Copyright (C) 2016 - 2020 Denis Zetzmann d1z@gmx
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * The Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file class_portfolio.c
 * @brief routines that implement a class-like struct which represents 
 * portfolios 
 *
 * This file contains the definitions of the "class" portfolio
 * 
 * @author Denis Zetzmann
 */ 

#include <stdlib.h>
#include <stdio.h>
#include "arrays.h"
#include "bstrlib.h"
#include "debug.h"

#include "class_account.h"
#include "class_portfolio.h"
#include "class_portfolio_element_stocks.h"
#include "class_portfolio_element_cfdfut.h"
#include "class_portfolio_element_cfdcur.h"
#include "class_quote.h"
#include "class_market_list.h"
#include "datatypes.h"
#include "database.h"

static void class_portfolio_setMethods(class_portfolio* self);
static void class_portfolio_destroyImpl(class_portfolio *self);
static class_portfolio* class_portfolio_clonePortfolioImpl(const class_portfolio* original);
static void class_portfolio_printTableImpl(const class_portfolio* self);
static void class_portfolio_addElementImpl(class_portfolio* self, const void* the_element);
static void class_portfolio_addNewElementImpl(class_portfolio* self, const class_market* the_market, const signal_type longorshort, const float quantity, const unsigned int quotenr, const unsigned int indicatorpos, const char* signalstring, const class_accounts* the_account, const int marketListPosition, const int sl_list_idx);
static void class_portfolio_removeElementImpl(class_portfolio* self, unsigned int elementToRemove);
static unsigned int class_portfolio_getNrElementsImpl(const class_portfolio* self);
static unsigned int class_portfolio_getElementIndexImpl(const class_portfolio* self, bstring* symbol, bstring* buydate, bstring* signalname);
static void class_portfolio_savePortfolioDBImpl(const class_portfolio* self);
static void class_portfolio_loadPortfolioDBImpl(class_portfolio* self);
static bool class_portfolio_containsSymbolImpl(const class_portfolio* self, bstring* symbol);
static unsigned int class_portfolio_getNrElementsPerSymbolImpl(const class_portfolio* self, bstring* symbol);
static void class_portfolio_updatePortfolioImpl(class_portfolio* self);
static float class_portfolio_getPortfolioEquityImpl(const class_portfolio* self);
static float class_portfolio_getPortfolioRiskFreeEquityImpl(const class_portfolio* self);
static void class_portfolio_setMarketListPositionsImpl(class_portfolio* self, class_market_list* the_list);

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "private" parts of portfolio class. Do not touch your private parts. 
 * 
 * This struct holds the data that is considered to be private, e.g. cannot be
 * accessed outside this file (to do that use the getter/setter functions).
 * The struct is refered by an opaque pointer of "accounts" object
 * @see portfolio_class.h definition of portfolio class 
 * 
 */
///////////////////////////////////////////////////////////////////////////////
struct _portfolio_private{
	unsigned int nr_elements;	/**< nr of elements in portfolio */
	float equity;     /**< current value of portfolio */
	float risk_free_equity;  /**< value of portfolio if all elements were sold at current stop loss */
};
typedef struct _portfolio_private portfolio_private;


//////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "constructor" function for portfolio objects
 *
 * This function initializes memory and maps the functions to the placeholder
 * pointers of the struct methods.
 * 
 * @param name char ptr with identifier of portfolio
 * 
 * @return pointer to new object of class portfolio
*/
///////////////////////////////////////////////////////////////////////////////
class_portfolio* class_portfolio_init(char* name)
{
	class_portfolio* self = NULL;
	
	// allocate memory for object
	self = ZCALLOC(1, sizeof(class_portfolio));
	
	//allocate memory for pointer to element array
	self->elements = (class_portfolio_element**)ZCALLOC(1,sizeof(class_portfolio_element));
	
	// reserve memory for private parts
	self->portfolio_private = ZCALLOC(1, sizeof(portfolio_private));
	
	//allocate memory for rest of the object
 	self->name = init_1d_array_bstring(1);
	
	//init data
 	*self->name = bfromcstr(name);
    
    // init private data
	self->portfolio_private->nr_elements = 0;
    self->portfolio_private->equity = 0.0;
    self->portfolio_private->risk_free_equity = 0.0;
	
	// set methods
    class_portfolio_setMethods(self);
	
	return self;
	
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief internal function that sets methods for portfolio objects
 *
 * all member functions of the class are pointers to functions, that are set 
 * by this routine.
 * 
 * @param self pointer to portfolio struct
*/
////////////////////////////////////////////////////////////////////////////////
static void class_portfolio_setMethods(class_portfolio* self)
{
 	self->destroy = class_portfolio_destroyImpl;
	self->clone = class_portfolio_clonePortfolioImpl;
	self->printTable = class_portfolio_printTableImpl;
	self->addElement = class_portfolio_addElementImpl;
	self->addNewElement = class_portfolio_addNewElementImpl;
	self->removeElement = class_portfolio_removeElementImpl;
	self->getNrElements = class_portfolio_getNrElementsImpl;
	self->getElementIndex = class_portfolio_getElementIndexImpl;
	self->saveToDB = class_portfolio_savePortfolioDBImpl;  
    self->loadFromDB = class_portfolio_loadPortfolioDBImpl;
	self->containsSymbol = class_portfolio_containsSymbolImpl; 
	self->getNrElementsBySymbol = class_portfolio_getNrElementsPerSymbolImpl;
    self->updatePortfolioEquity = class_portfolio_updatePortfolioImpl;
    self->getPortfolioEquity = class_portfolio_getPortfolioEquityImpl;
    self->getPortfolioRiskFreeEquity = class_portfolio_getPortfolioRiskFreeEquityImpl;
    self->setMarketListPositions = class_portfolio_setMarketListPositionsImpl;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief "destructor" function for portfolio objects
 * 
 * This function frees the memory consumed by portfolio objects
 * 
 * @param self pointer to portfolio object
*/
///////////////////////////////////////////////////////////////////////////////
static void class_portfolio_destroyImpl(class_portfolio *self)
{
	//destroy all included portfolio_elements
	for(unsigned int i=0; i<self->portfolio_private->nr_elements; i++)
	{
		self->elements[i]->destroy(self->elements[i]);
	}
	free(self->elements);
	
	// free private data
	free(self->portfolio_private);
	
	
	// free public data
	free_1d_array_bstring(self->name,1);
	free(self);    
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief clone a portfolio object
 *
 * clone complete portfolio object (deep copy original into clone), if 
 * original object gets modified or destroy()ed, the clone will still live
 * @param original portfolio pointer to original 
 * @returns portfolio pointer to object that will hold copy of original
 */
///////////////////////////////////////////////////////////////////////////////
static class_portfolio* class_portfolio_clonePortfolioImpl(const class_portfolio* original)
{
	class_portfolio* clone = NULL;
	
	// first "clone" object with basic init data
	clone = class_portfolio_init(bdata(*original->name));
	
	// now copy portfolio_elements
 	for(unsigned int i = 0; i < original->portfolio_private->nr_elements; i++)
		clone->addElement(clone, original->elements[i]);

	return clone;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief adds existing portfolio_element object to portfolio
 * 
 * Add an existing portoflio_element to the given portfolio
 * 
 * @param self pointer to portfolio object
 * @param the_element pointer to portfolio_element object
*/
////////////////////////////////////////////////////////////////////////////
static void class_portfolio_addElementImpl(class_portfolio* self, const void* the_element)
{
    // first cast the element to base class until we know which type of portfolio element it really is
    class_portfolio_element* tmp_element_base = (class_portfolio_element*) the_element;
    
    if(biseqcstr(*tmp_element_base->markettype, "STOCK"))
    {
        // first upcast void pointer of argument to right type
        class_portfolio_element_stocks* tmp_element = (class_portfolio_element_stocks*) the_element;
        
        // increase nr of elements in portfolio object
        self->portfolio_private->nr_elements = self->portfolio_private->nr_elements + 1;
        
        // realloc indicator vector for new element object
        self->elements = (class_portfolio_element**)realloc(self->elements, (self->portfolio_private->nr_elements + 1) * sizeof(class_portfolio_element));
        if(self->elements == NULL)
        {
            log_err("Memory allocation failed");
            exit(EXIT_FAILURE);
        }        
        // clone given element within portfolio object
        self->elements[self->portfolio_private->nr_elements - 1] = tmp_element->clone(the_element);
        
        return;
    }
    else if(biseqcstr(*tmp_element_base->markettype, "CFDFUT"))
    {
        class_portfolio_element_cfdfut* tmp_element = (class_portfolio_element_cfdfut*) the_element;
        
        // increase nr of elements in portfolio object
        self->portfolio_private->nr_elements = self->portfolio_private->nr_elements + 1;
        
        // realloc indicator vector for new element object
        self->elements = (class_portfolio_element**)realloc(self->elements, (self->portfolio_private->nr_elements + 1) * sizeof(class_portfolio_element));
        if(self->elements == NULL)
        {
            log_err("Memory allocation failed");
            exit(EXIT_FAILURE);
        } 
        
        // clone given element within portfolio object
        self->elements[self->portfolio_private->nr_elements - 1] = tmp_element->clone(the_element);
        
        return;
    }
    else if(biseqcstr(*tmp_element_base->markettype, "CFDCUR"))
    {
        class_portfolio_element_cfdcur* tmp_element = (class_portfolio_element_cfdcur*) the_element;
        
        // increase nr of elements in portfolio object
        self->portfolio_private->nr_elements = self->portfolio_private->nr_elements + 1;
        
        // realloc indicator vector for new element object
        self->elements = (class_portfolio_element**)realloc(self->elements, (self->portfolio_private->nr_elements + 1) * sizeof(class_portfolio_element));
        if(self->elements == NULL)
        {
            log_err("Memory allocation failed");
            exit(EXIT_FAILURE);
        } 
        
        // clone given element within portfolio object
        self->elements[self->portfolio_private->nr_elements - 1] = tmp_element->clone(the_element);
        
        return;
    }
    else
    {
        class_portfolio_element* tmp_element = (class_portfolio_element*) the_element;
        log_err("Unknown markettype %s for symbol %s", bdata(*tmp_element->markettype), bdata(*tmp_element->symbol));
        
        // cleanup what is possible and exit
        self->destroy(self);
        tmp_element->destroy(tmp_element);        
        exit(EXIT_FAILURE);
    }
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief adds new portfolio_element object to portfolio
 * 
 * Adds new portoflio_element to the given portfolio, using given parameters
 * 
 * @param self pointer to portfolio object
 * @param longorshort	signal_type (long/short) for new element
 * @param quantity	float with quantity bought
 * @param quotenr use this quote/date/daynr dataset
 * @param indicatorpos position of the indicator within market object that
 * 			triggered the signal
 * @param signalstring char ptr with signal name
 * @param the_account pointer to accounts object 
*/
////////////////////////////////////////////////////////////////////////////
static void class_portfolio_addNewElementImpl(class_portfolio* self, const class_market* the_market, const signal_type longorshort, 
					const float quantity, const unsigned int quotenr, const unsigned int indicatorpos, 
					const char* signalstring, const class_accounts* the_account, const int marketListPosition, const int sl_list_idx)
{
	// increase nr of elements in portfolio object
	self->portfolio_private->nr_elements = self->portfolio_private->nr_elements + 1;
	
    if(biseqcstr(*the_market->markettype, "STOCK"))
    {
        // realloc indicator vector for new element object
        self->elements = (class_portfolio_element**)realloc(self->elements, (self->portfolio_private->nr_elements + 1) * sizeof(class_portfolio_element));
        if(self->elements == NULL)
        {
            log_err("Memory allocation failed");
            exit(EXIT_FAILURE);
        } 
        
        // create temporary portfolio element
        class_portfolio_element_stocks* tmp_element_stocks = NULL;
        tmp_element_stocks = class_portfolio_element_stocks_init(the_market, longorshort, quantity, quotenr, indicatorpos, signalstring, the_account, marketListPosition, sl_list_idx);
        
        // calculate right values for the new element
        tmp_element_stocks->update(tmp_element_stocks, 0.0, true, 0, the_market, quotenr, the_account->getCash(the_account));
        //tmp_element->printTable(tmp_element);
        
        // clone given element within portfolio object
        self->elements[self->portfolio_private->nr_elements - 1] = tmp_element_stocks->clone(tmp_element_stocks);
        
        tmp_element_stocks->destroy(tmp_element_stocks);    
        return;
    }
    else if(biseqcstr(*the_market->markettype, "CFDFUT"))
    {
        // realloc indicator vector for new element object
        self->elements = (class_portfolio_element**)realloc(self->elements, (self->portfolio_private->nr_elements + 1) * sizeof(class_portfolio_element));
        if(self->elements == NULL)
        {
            log_err("Memory allocation failed");
            exit(EXIT_FAILURE);
        } 
        
        // create temporary portfolio element
        class_portfolio_element_cfdfut* tmp_element_cfdfut = NULL;
        tmp_element_cfdfut = class_portfolio_element_cfdfut_init(the_market, longorshort, quantity, quotenr, indicatorpos, signalstring, the_account, marketListPosition, sl_list_idx);
   
        // calculate right values for the new element
        tmp_element_cfdfut->update(tmp_element_cfdfut, 0.0, true, 0, the_market, quotenr, the_account->getCash(the_account));
        //tmp_element->printTable(tmp_element);
        
        // clone given element within portfolio object
        self->elements[self->portfolio_private->nr_elements - 1] = tmp_element_cfdfut->clone(tmp_element_cfdfut);
        
        tmp_element_cfdfut->destroy(tmp_element_cfdfut);    
        return;
    }
    
    else if(biseqcstr(*the_market->markettype, "CFDCUR"))
    {
        // realloc indicator vector for new element object
        self->elements = (class_portfolio_element**)realloc(self->elements, (self->portfolio_private->nr_elements + 1) * sizeof(class_portfolio_element));
        if(self->elements == NULL)
        {
            log_err("Memory allocation failed");
            exit(EXIT_FAILURE);
        } 
        
        // create temporary portfolio element
        class_portfolio_element_cfdcur* tmp_element_cfdcur = NULL;
        tmp_element_cfdcur = class_portfolio_element_cfdcur_init(the_market, longorshort, quantity, quotenr, indicatorpos, signalstring, the_account, marketListPosition, sl_list_idx);
        
        // calculate right values for the new element
        tmp_element_cfdcur->update(tmp_element_cfdcur, 0.0, true, 0, the_market, quotenr, the_account->getCash(the_account));
        
        // clone given element within portfolio object
        self->elements[self->portfolio_private->nr_elements - 1] = tmp_element_cfdcur->clone(tmp_element_cfdcur);
        
        tmp_element_cfdcur->destroy(tmp_element_cfdcur);    
        return;
    }   
    else
    {
        log_err("Unknown markettype %s for symbol %s", bdata(*the_market->markettype), bdata(*the_market->symbol));
        exit(EXIT_FAILURE);    
    }
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief removes an existing portfolio_element from portfolio
 * 
 * This method removes an existing portfolio_element from portfolio, elements
 * after the removed one move up by 1 index nr (leaving no holes)
 * 
 * @param self pointer to portfolio object
 * @param elementToRemove uint with index of portfolio_element to remove
*/
////////////////////////////////////////////////////////////////////////////
static void class_portfolio_removeElementImpl(class_portfolio* self, unsigned int elementToRemove)
{
	// check if elementToRemove really is within array limits
	check(elementToRemove <= (self->portfolio_private->nr_elements -1), "Trying to remove element with index nr %u failed, \nportfolio \"%s\" has only %u elements (starting with index 0)!", errorlabel,
	     elementToRemove,  bdata(*self->name), self->portfolio_private->nr_elements);
	
	// create a temporary vector of portfolio elements
	class_portfolio_element **tempElements = NULL;
	
	// alloc memory for 1 element less than original list
	tempElements = (class_portfolio_element**)ZCALLOC((self->portfolio_private->nr_elements-1), sizeof(class_portfolio_element));
	
	// copy everything before the elementToRemove
	if(elementToRemove != 0)
	{
		for(unsigned int i=0; i<elementToRemove; i++)
			tempElements[i] = self->elements[i]->clone(self->elements[i]);
	}
	
	// copy everything after the elementToRemove
	if(elementToRemove != (self->portfolio_private->nr_elements - 1))
	{
		for(unsigned int i=elementToRemove + 1; (i<self->portfolio_private->nr_elements); i++)
			tempElements[i-1] = self->elements[i]->clone(self->elements[i]);
	}
	
	//destroy all originally included portfolio_element objects
	for(unsigned int i=0; i<self->portfolio_private->nr_elements; i++)
	{
		self->elements[i]->destroy(self->elements[i]);
	}
	free(self->elements);
	
	// redirect pointer to portfolio_elements to freshly copied list
	self->elements = tempElements;
	
	// decrease element counter
	self->portfolio_private->nr_elements = self->portfolio_private->nr_elements - 1;
	
	return;

errorlabel:
	self->destroy(self);
	exit(EXIT_FAILURE);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief print portfolio information to terminal
 * 
 * prints tabular portfolio information to terminal, including all portfolio
 * elements
 * 
 * @param self pointer to portfolio object
*/
////////////////////////////////////////////////////////////////////////////
static void class_portfolio_printTableImpl(const class_portfolio* self)
{
	// the table I´d like to create should look like the following, and should adapt to the number of digits 
	// -----------------------------------------------------------------------------------------------------------------------
	// symbol | mkttype | buydate   | type  | SL       | quant | buyqte   | lastqte  | P/L       | P/L@sl      | days | signal
	// -----------------------------------------------------------------------------------------------------------------------
	// DJI    | CFDFUT | 2018-01-24 | short | 23500.00 | 10.00 | 26252.12 | 23860.46 | +16747.14 |   +19690.41 |    2 | Posi 1
	// DAX    | CFDFUT | 2017-11-23 | long  | 13010.00 | 10.00 | 13008.55 | 13023.98 |   +154.30 |      +14.50 |    2 | Posi 2
	// EURJPY | CFDCUR | 2017-12-08 | long  |  135.000 |  1.00 |  133.510 |  132.330 |   -891.70 |    +1125.98 |    2 | Posi 3
	// EURUSD | CFDCUR | 2017-12-18 | long  |  1.17000 |  1.00 |  1.17820 |  1.18690 |   +733.00 |     -690.88 |    2 | Posi 4	

	// following variables will store maximum string length to format everything into a nice table
	int symbollength=0, idlength=0, marketlength=0, sllength=0, quantlength=0, buyqtlength=0, lastqtlength=0, pllength=0, rfpllength=0, dayslength=0, signallength=0, buyqtmarketdigits=0, currqtaccountdigits=0, buyqtaccountdigits=0;


	printf("\n%s, %u elements", bdata(*self->name), self->getNrElements(self));
	
	// loop for whole portfolio and find the largest strings, store their lengths
	// warning: that most likely is the ugliest code I ever wrote :(
	for(unsigned i=0; i<self->portfolio_private->nr_elements; i++)
	{
		if(blength(*self->elements[i]->symbol) >  symbollength) 
			symbollength = blength(*self->elements[i]->symbol);
        if(blength(*self->elements[i]->identifier) > idlength)
            idlength = blength(*self->elements[i]->identifier);
		if(blength(*self->elements[i]->markettype) >  marketlength)
			marketlength = blength(*self->elements[i]->markettype);
		if((snprintf(NULL, 0, "%.0f", self->elements[i]->stoploss) + self->elements[i]->Buyquote_market->nr_digits) > (unsigned int) sllength)		// snprintf(NULL, 0, "%.0f", self->elements[i]->stoploss) gets length of float as string (without digits)
			sllength = snprintf(NULL, 0, "%.0f", self->elements[i]->stoploss) + self->elements[i]->Buyquote_market->nr_digits;
		if(snprintf(NULL, 0, "%.2f", self->elements[i]->quantity) >  quantlength)
			quantlength = snprintf(NULL, 0, "%.2f", self->elements[i]->quantity);
		if((snprintf(NULL, 0, "%.0f", *self->elements[i]->Buyquote_market->quotevec) + self->elements[i]->Buyquote_market->nr_digits) > (unsigned int) buyqtlength)
			buyqtlength = snprintf(NULL, 0, "%.0f", *self->elements[i]->Buyquote_market->quotevec) + self->elements[i]->Buyquote_market->nr_digits;	// my brain hurted writing that :(
		if((snprintf(NULL, 0, "%.0f", *self->elements[i]->currQuote_market->quotevec) + self->elements[i]->currQuote_market->nr_digits) > (unsigned int) lastqtlength)
			lastqtlength = snprintf(NULL, 0, "%.0f", *self->elements[i]->currQuote_market->quotevec) + self->elements[i]->currQuote_market->nr_digits;	
		if((snprintf(NULL, 0, "%.0f", self->elements[i]->p_l) + self->elements[i]->Buyquote_market->nr_digits) > (unsigned int) pllength)
		{
			pllength = snprintf(NULL, 0, "%.0f", self->elements[i]->p_l) + self->elements[i]->Buyquote_market->nr_digits;	
			pllength = pllength + 2;
		}
		if((snprintf(NULL, 0, "%.0f", self->elements[i]->risk_free_p_l) + self->elements[i]->Buyquote_market->nr_digits) > (unsigned int) rfpllength)
		{
			rfpllength = snprintf(NULL, 0, "%.0f", self->elements[i]->risk_free_p_l) + self->elements[i]->Buyquote_market->nr_digits;	// this is what I think coding in brainfuck feels like
			rfpllength = rfpllength + 2;
		}
		if(snprintf(NULL, 0, "%i", self->elements[i]->trading_days) > dayslength)
			dayslength = snprintf(NULL, 0, "%i", self->elements[i]->trading_days);
		if(blength(*self->elements[i]->signalname) > signallength)
			signallength = blength(*self->elements[i]->signalname);
		if(self->elements[i]->Buyquote_market->nr_digits > (unsigned int) buyqtmarketdigits)
			buyqtmarketdigits = self->elements[i]->Buyquote_market->nr_digits;
		if(self->elements[i]->currQuote_market->nr_digits > (unsigned int) currqtaccountdigits)
			currqtaccountdigits = self->elements[i]->currQuote_market->nr_digits;
		if(self->elements[i]->Buyquote_account->nr_digits > (unsigned int) buyqtaccountdigits)
			buyqtaccountdigits = self->elements[i]->Buyquote_account->nr_digits;
	}
	
	// correct collumn width if too narrow to hold headings
	if(symbollength < 6)
    {
        symbollength = 6;
    }
    if(idlength < 10)
    {
        idlength = 10;
    }
	if(dayslength < 4) 		
    {
		dayslength = 4;	
    }
	if(quantlength < 5)
    {
		quantlength = 5;
    }

	unsigned int totallength = snprintf(NULL, 0, "%u",self->portfolio_private->nr_elements-1) + symbollength + idlength + marketlength + 10 + 5 + sllength + quantlength + buyqtlength + lastqtlength + pllength + rfpllength + dayslength + signallength;
	totallength = totallength + buyqtmarketdigits + currqtaccountdigits + buyqtaccountdigits;  // add space for digits
	totallength = totallength + 33; // add 33 spaces for " | " pattern
	
	// now printf the heading:
	// ------------------------------------------------------------------------------------------------------------------------------------
	// symbol | identifier | mkttype | buydate   | type  | SL       | quant | buyqte   | lastqte  | P/L       | P/L@sl      | days | signal
	// ------------------------------------------------------------------------------------------------------------------------------------
	printf("\n");
	for(unsigned k=0; k<totallength+1; k++)
		printf("=");
	
	printf("\n%-*s | %-*s | %-*s | %-*s | %-9s | %-5s | %-*s | %-*s | %-*s | %-*s | %-*s | %-*s | %-*s | %-*s\n", 
		snprintf(NULL, 0, "%u",self->portfolio_private->nr_elements-1), "idx",
		symbollength, "symbol", 
        idlength, "identifier",
		marketlength, "mkttype",
		"buydate", 
		"type", 
		sllength+1, "SL", 
		quantlength, "quant",
		buyqtlength+1, "buyqte", 
		lastqtlength+1, "last", 
		pllength, "P/L", 
		rfpllength, "P/L@sl", 
		dayslength, "days", 
		signallength, "signal");

	for(unsigned k=0; k<totallength+1; k++)
		printf("-");
	
	// convert type to string
	for(unsigned i=0; i<self->portfolio_private->nr_elements; i++)
	{
		bstring typestring=NULL;
		switch(self->elements[i]->type)
		{
			case longsignal:
				typestring=bfromcstr("long");
				break;
			case shortsignal:
				typestring=bfromcstr("short");
				break;
			case undefined:
			default: 
				typestring=bfromcstr("undefined");			
				break;
		}
	
		// now line by line print the portfolio elements like this:
		// DJI    | CFDFUT | 2018-01-24 | short | 23500.00 | 10.00 | 26252.12 | 23860.46 | +16747.14 |   +19690.41 |    2 | Posi 1
		printf("\n[%-*u] | %-*s | %-*s | %-*s | %s | %-5s | %*.*f | %*.2f | %*.*f | %*.*f | %+*.*f | %+*.*f | %*i | %-*s", 
			snprintf(NULL, 0, "%u",self->portfolio_private->nr_elements-1),i,
			symbollength, bdata(*self->elements[i]->symbol), 
            idlength, bdata(*self->elements[i]->identifier),
			marketlength, bdata(*self->elements[i]->markettype),
			bdata(*self->elements[i]->Buyquote_market->datevec),
			bdata(typestring),
		        sllength+1, self->elements[i]->Buyquote_market->nr_digits, self->elements[i]->stoploss,
			quantlength, self->elements[i]->quantity,
			buyqtlength+1, self->elements[i]->Buyquote_market->nr_digits, *self->elements[i]->Buyquote_market->quotevec,
			lastqtlength+1, self->elements[i]->currQuote_market->nr_digits, *self->elements[i]->currQuote_market->quotevec,
			pllength, self->elements[i]->Buyquote_account->nr_digits, self->elements[i]->p_l,
			rfpllength, self->elements[i]->Buyquote_account->nr_digits, self->elements[i]->risk_free_p_l,
			dayslength, self->elements[i]->trading_days,
			signallength, bdata(*self->elements[i]->signalname));

		// phew... lets get out of here :( Note to myself: find a nice terminal output library
		bdestroy(typestring);
	}
    printf("\n");
    for(unsigned k=0; k<totallength+1; k++)
		printf("=");
	printf("\n");
    	
//     printf("Equity: %.2f, risk free Equ.: %.2f", self->portfolio_private->equity, self->portfolio_private->risk_free_equity);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief return the nr of portfolio entries
 * 
 * This method returns the nr of entries within portfolio object
 * 
 * @param self pointer to portfolio object
 * @returns nr of elements as uint
*/
////////////////////////////////////////////////////////////////////////////
static unsigned int class_portfolio_getNrElementsImpl(const class_portfolio* self)
{
	return self->portfolio_private->nr_elements;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief return the index/position of a certain entry within portfolio
 * 
 * This method returns the index/position of a certain entry within portfolio
 * 
 * @param self pointer to portfolio object,
 * @param *symbol bstring ptr to symbol
 * @param *buydate bstring ptr 
 * @param *signalname bstring ptr
 * @returns index of element as uint
*/
////////////////////////////////////////////////////////////////////////////
static unsigned int class_portfolio_getElementIndexImpl(const class_portfolio* self, bstring* symbol, bstring* buydate, bstring* signalname)
{
	unsigned int position = 0;
	
	for(position = 0; position < self->portfolio_private->nr_elements; position++)
	{
		if(biseq(*buydate, *self->elements[position]->Buyquote_account->datevec) && biseq(*signalname, *self->elements[position]->signalname) && biseq(*symbol, *self->elements[position]->symbol))
			return position;
	}
	
	// if we did not escape the loop above, there is no portfolio element with the given characteristics
	log_err("\nNo portfolio element with buydate %s and signalname %s found within portfolio \"%s\"\n", 
		bdata(*buydate), bdata(*signalname), bdata(*self->name));
	
	exit(EXIT_FAILURE);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief store portfolio object in database
 * 
 * This method stores the portfolio object in database
 * 
 * @param self pointer to portfolio object
*/
////////////////////////////////////////////////////////////////////////////
static void class_portfolio_savePortfolioDBImpl(const class_portfolio* self)
{
	// clear existing database table, for sold items should not stay in the database
	// by updating the table as a whole at the end of program runtime, 
	// it will be way faster compared to updating db for every single buy/sell
	db_mysql_clear_portfolio_table();
	for(unsigned int i = 0; i< self->portfolio_private->nr_elements; i++)
		db_mysql_update_portfolio(self->elements[i]);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief load portfolio object from database
 * 
 * This method loads an portfolio object from database
 * 
 * @param self pointer to portfolio object
*/
////////////////////////////////////////////////////////////////////////////
static void class_portfolio_loadPortfolioDBImpl(class_portfolio* self)
{
    db_mysql_get_portfolio(self);
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief check if a specific market is in portfolio
 * 
 * This method checks if a specific market is already in the portfolio
 * 
 * @param self pointer to portfolio object
 * @param *symbol bstr ptr
 * @returns true if found, false otherwise
*/
////////////////////////////////////////////////////////////////////////////
static bool class_portfolio_containsSymbolImpl(const class_portfolio* self, bstring* symbol)
{
	bool symbolFound = false;
	
	// prevent segfaulting when portfolio is empty
	if(self->portfolio_private->nr_elements == 0)
		return symbolFound;
	
	for(unsigned int i = 0; i< self->portfolio_private->nr_elements; i++)
	{
		if(biseqcstr(*self->elements[i]->symbol , bdata(*symbol)))
		{
			symbolFound = true;
			break;
		}
	}
	
	return symbolFound;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief check how many entries of specific markets are within portfolio
 * 
 * This method checks how many entries of a specific markets are within 
 * portfolio
 * 
 * @param self pointer to portfolio object
 * @param *symbol bstr ptr
 * @returns nr of elements found as uint
*/
////////////////////////////////////////////////////////////////////////////
static unsigned int class_portfolio_getNrElementsPerSymbolImpl(const class_portfolio* self, bstring* symbol)
{
	unsigned int elementsFound = 0;
	
	// prevent segfaulting when portfolio is empty
	if(self->portfolio_private->nr_elements == 0)
		return elementsFound;
	
	for(unsigned int i = 0; i< self->portfolio_private->nr_elements; i++)
	{
		if(biseqcstr(*self->elements[i]->symbol , bdata(*symbol)))
		{
			elementsFound++;
		}
	}
	
	return elementsFound;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief update private data of portfolio: equity and risk free equity
 * 
 * This method updates the private date of the portfolio: equity and risk free
 * equity, based on the value of all portfolio elements
 * 
 * @param self pointer to portfolio object
*/
////////////////////////////////////////////////////////////////////////////
static void class_portfolio_updatePortfolioImpl(class_portfolio* self)
{
    float equity = 0.0;
    float risk_free_equity = 0.0;
    
    for (unsigned int i=0; i<self->portfolio_private->nr_elements; i++)
    {
        equity = equity + self->elements[i]->equity;
        risk_free_equity = risk_free_equity + self->elements[i]->rf_equity;
    }
    
    self->portfolio_private->equity = equity;
    self->portfolio_private->risk_free_equity = risk_free_equity;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter method for portfolio equity
 * 
 * This method returns the current portoflio equity (which is not accessible 
 * from the outside of this file)
 * 
 * @param self pointer to portfolio object
 * @returns equity as float
*/
////////////////////////////////////////////////////////////////////////////
static float class_portfolio_getPortfolioEquityImpl(const class_portfolio* self)
{
    return self->portfolio_private->equity;
}

///////////////////////////////////////////////////////////////////////////////
/** 
 * @brief getter method for portfolio risk free equity
 * 
 * This method returns the current portoflio risk free equity (which is not 
 * accessible from the outside of this file)
 * 
 * @param self pointer to portfolio object
 * @returns risk free equity as float
*/
////////////////////////////////////////////////////////////////////////////
static float class_portfolio_getPortfolioRiskFreeEquityImpl(const class_portfolio* self)
{
    return self->portfolio_private->risk_free_equity;
}

///////////////////////////////////////////////////////////////////////////////
/**
 * @brief determine and set positions of each portfolio entry´s market within
 * 		  a given market_list
 *
 * This method determines and sets the positions of each portfolio entry´s 
 * market within a given market_list.
 * @Note Usually when creating a portfolio_element object (using 
 * portfolio_element methods or portfolio´s methods), the caller of the 
 * constructor is responsible for determining the right position and giving it
 * to the constructor as argument. However, it might be neccessary to set the
 * positions again using this method (for example after reading the portfolio
 * from a database, as the database function has no clue about market_lists
 * @param self ptr to portfolio object
 * @param the_list ptr to market_list object
 */
///////////////////////////////////////////////////////////////////////////////
static void class_portfolio_setMarketListPositionsImpl(class_portfolio* self, class_market_list* the_list)
{
	for(unsigned int i=0; i<self->portfolio_private->nr_elements; i++)
	{
		int position = -1;
		position = the_list->getElementIndex(the_list, bdata(*self->elements[i]->symbol));
		self->elements[i]->setMarketListPosition(self->elements[i], position);
		if(position == -1)
        {
			log_warn("Portfolio element %s not part of market list %s. Please check market configuration!", bdata(*self->elements[i]->symbol), bdata(*the_list->name));
        }
	}
}

// eof
