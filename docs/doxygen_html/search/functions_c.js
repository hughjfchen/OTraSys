var searchData=
[
  ['resize_5f1d_5farray_5fbstring',['resize_1d_array_bstring',['../arrays_8c.html#a059afbd800c3ac2906b47584bf791e23',1,'resize_1d_array_bstring(bstring *array, int newsize):&#160;arrays.c'],['../arrays_8h.html#a059afbd800c3ac2906b47584bf791e23',1,'resize_1d_array_bstring(bstring *array, int newsize):&#160;arrays.c']]],
  ['resize_5f1d_5farray_5ffloat',['resize_1d_array_float',['../arrays_8c.html#abdcf8e5c892554fdbde198f78f365397',1,'resize_1d_array_float(float *array, int newsize):&#160;arrays.c'],['../arrays_8h.html#abdcf8e5c892554fdbde198f78f365397',1,'resize_1d_array_float(float *array, int newsize):&#160;arrays.c']]],
  ['resize_5f1d_5farray_5fuint',['resize_1d_array_uint',['../arrays_8c.html#ace0ca712d8e56f7ca9cf2ca5f417806e',1,'resize_1d_array_uint(unsigned int *array, int newsize):&#160;arrays.c'],['../arrays_8h.html#ace0ca712d8e56f7ca9cf2ca5f417806e',1,'resize_1d_array_uint(unsigned int *array, int newsize):&#160;arrays.c']]],
  ['resize_5f2d_5farray_5ffloat',['resize_2d_array_float',['../arrays_8c.html#a19cffa80b91450a32481890f96742f77',1,'resize_2d_array_float(float **array, unsigned int oldN, unsigned int oldM, unsigned int newN, unsigned int newM):&#160;arrays.c'],['../arrays_8h.html#a19cffa80b91450a32481890f96742f77',1,'resize_2d_array_float(float **array, unsigned int oldN, unsigned int oldM, unsigned int newN, unsigned int newM):&#160;arrays.c']]],
  ['runpersistence',['RunPersistence',['../classp1d_1_1Persistence1D.html#afa9e076d614ac0f0e8dd7e63b1c81868',1,'p1d::Persistence1D']]]
];
