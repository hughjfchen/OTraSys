<img src="pics/logo.png" alt="OTraSys Logo" style="width:50px ; float:left"/>

OTraSys- The Open Trading System  
An open source framework to create trading systems  

README
======
This document describes various aspects of this software, adressed to both 
users and developers.
The contents of this file:

  * [Preface](#Preface): warm words to the bold user
  * [What it isn't](#whatsnot): What you should not expect (at the moment)
  * [What it is](#whatitis): what you definitely can expect
  * [What it might become](#become): what might be if santa brings me a bag of patience
  * [Intented users](#intended_users): what is the targeted audience of this program
  * [Software architecture](#architecture): a quick overview over the functional blocks
  
Preface: <a name="Preface"></a>
========
This is release 0.5.1 of OTraSys. As such, it is software still its very early stages. It
has bugs that I know of. It has bugs that I do not (yet) know of. I will 
continually work on this piece and invite everybody to contribute.

What it isn`t <a name="whatsnot"></a>
=============

#### Click and forget
It is a Linux console program. At the moment there is no gui. You will need a 
console and basic knowledge of using it. At the moment there is even no 
Windows build (maybe in the near future if enough people are interested).  


#### Complete trading system with fool-proof portfolio management out of the box
Risk management on a portfolio level was introduced with v0.2.0. Allocation now
depends on correlation between all markets. This algorithm is in its very early
stage and very likely to change in the future.  
Money management and currency translation of different markets to you account´s
currency was implemented in v0.3. The allocation algorithm now thinks in terms 
of "risk in account currency".  
Transaction Costs were introduced in v0.4.1. You now can model your broker´s cost
structure as this will greatly influence the performance over time.  
Currently there is no possibility to connect `OTraSys` to your broker and
execute all signals automatically. You can execute all signals manually, of course.
This might change in the future. Honestly, do you trust me enough to connect your
hard earned money to a software/website you just stumbled upon in the internet?
DON`T DO THAT! ;-)
By the way, I am a fan of the UNIX philosophy "one job, one tool". So the system
concentrates on the number crunching. Fetching/preparing data is a different job.
Doing evaluations of the orderbook is a different job. So for pre-/and postprocessing
you should look for additional tools.

#### Data provider  
From v0.5.1 OTraSys relies on two different databases: the system database 
where all data that is specific for a single instance of an OTraSys trading 
system is located. And a central quote repository, which provides quote data 
for all OTraSys instances. Read /docs/database_structure for more information
on that.
OTraSys itself does not pull in data from any online sources. It is be the
users responsibility to fetch and prepare the price data and fill the quote
repository with that. However, as a starter, the script 
`/tools\update_quotes.py` will read the config and symbol file, look for .csv
files under `/Data` and fill the quote repository with that data. You can use 
Use that script as a starting point to connect OTraSys to your own data feeds.

#### Proprietary, black box  
Look in `src/` You will find everything there is about my program (and it is not very
pretty, I admit :)) I do hope that you got a copy of the source code, otherwise
someone violated the licence. In this case you can find the sourcecode on  
https://gitlab.com/werwurm/OTraSys.git  
If you don't trust me (and again, you shouldn`t if you want to trade your real
hard-earned money), you can see how the software works. I tried to document each
algorithm extensively. If you want to add features, you can do so (please consider
giving your additions back to the community). Everything is under your control.


#### highly optimised  
As mentioned, the software is in an very early stage. Main focus was on creating
something that compiles and is useful. Absolutely no effort has been made to do 
any optimisations. However the step from v0.3 to v0.4.0 marked a huge effort (that
took more that 2 years of my sparetime) which basically resulted in a more or less
complete rewrite of the core. The source now uses an object-oriented approach while
still being pure C code. Everything is now done in memory, interaction with the 
database happens only at startup and in the end of the programm.   
The program is written in C and is as such already pretty fast, given the lack of 
optimisation. However, there are a lot of tweaks that come to my mind when thinking 
how to make it even faster.


What it is <a name="whatitis"></a>
==========
#### Starting point for a complete trading system
As the heading contradicts somewhat the second bullet of "What it isn't", I am
somewhat proud of having the foundation of everything I want from my system 
implemented. Of course there is enough spit-and-polish left to do, a lot of 
features are on my to-do-list, but here is what the system is already capable of
to do:
  * fully customizable automated Ichimoku Kinko Hyo Signal evaluation
  * additional (optional) market filters
  * arbitrary number of markets: you have the data, the system can do the number
    crunching!
  * v0.3 introduced currencies to the system: the concept of an account currency
    and markets in optionally different currencies that can be translated into 
    account currency (given that you have data with conversion rates)
  * customizable portfolio allocation algorithm to balance the position size of
    new trades against existing positions in portfolio
  * customizable stop losses: chose a simple percentage stop or a volatility-based
    chandelier stop loss
  * automated recalculation of stop losses each day
  * portfolio and backtest/orderbook statistics
  * performance tracking
  * incorporating transaction costs into performance

#### Signal generator for ichimoku entries and exits  
Always stared at Ichimoku charts to see the signals? Manually traced all 
components if they do cross somewhere? Did that every evening for dozens of 
markets? Let your computer do the boring math. Choose your markets, feed
your data in and push enter. See the signals, execute them. Alter the stop
loss strategy to your needs.


#### capable to do backtests  
Feed not only the last 100 oder 200 days, but the price data for the last,
say, 10 years. Again, start the program. It is fast enough you won't make
a coffee in the time of a 10year backtest (Remark: this does not apply
to those shiny capsule machines- in fact you might be able to have one
during system run time- but honestly this is no real coffee :))


#### open and free: source code, databases, data import/export with .csv 
As stated above: free and open source! Contribute if you like, use it for
yourself if you want! (fix my bugs ;))
With v0.2.0 a lot of effort went into a full documentation of the sources
in a doxygen compatible way- the full hyperlinked html/pdf reference now 
is part of /docs  
  
#### capable to prepare candlestick plots files for processing with gnuplot  
You do not have to stare at charts for hours. But still can, if you want :)
Feed the market data of 50 markets into the system, plot the chart and see
the "market weather" at one glance. Also great to check new signal implementations
for sanity. (Well, honestly charts are still pretty...eh ugly :) )


#### hobby project
If you read this far: kudos! I develop OTraSys in my sparetime. I do
not want to spend my evenings staring at charts and reading through tons
of market comments. (So I traded that for programming- hour after hour, 
day after day). I definitely wil trade the signals with my own money, continue
to feed my blog with signals and charts, expanding my software until it fully
fits my needs (which are a moving target, of course).



What it might become <a name="become"></a>
====================

#### framework that allows rapid prototyping of trading systems  
This is my ultimate goal. When I started (don't remember why I did chose C at
the beginning), I had to create the whole infrastructure for a trading system.
As the code evolved, it became more clearly that I could abstract and generalize
a lot of the code. Ultimately, I want the system to become a framework for
the creation of trading systems. As you can see in the architecture below, a 
lot of effort went into the separation of the infrastructure from the trading
system components.


#### highly optimised and faster than it is now (given that it is pretty fast considering the 0 optimisations)  
What is fast should get faster. When time and feature wishlist allow, I will 
take some time to optimize the code. A good candidate are the database queries
which are executed day for day at the moment. No need for that, given that
those queries are the most time consuming part of the program.


#### an active project where users share their code and ideas for trading systems 
Oh... that would be neat :) My first hope is to turn the one-man-show into a 
two or three-man (/woman) show.


#### using a web-based gui to lower the entry barrier  
A lot of good old terminal programs have web guis nowadays. As this is more or
less eye-candy for me, I acknowledge the fact that the linux console/mysql 
knowlegde are some kind of entry barrier for new and unexperienced users 
(I am NOT talking about trading experience).


#### memory leak free :))  
While that statement will be universally true forever, the software got way
better in that regard. During the switch from v0.3 to v0.4 and the oop-approach
I got rid of most of the leaks. Before that, I have to admit valgrind output 
was like ...compiler lord of the rings I-III.
There are still some leaks due to improper handling of strings, I guess. Nothing
tragic, but I will be working on it... soon :)



Intended users <a name="intended_users"></a>
==============

To use OTraSys in its current state you should be:
  * bold to use linux, it`s command line and with at least very basic knowlegde of mysql  
  * curious to play with the various config file options  
  * best case: programmer familiar with C and willing to contribute :)  

  
(c) 2016-2020 Denis Zetzmann  
d1z@gmx.de  
www.spare-time-trading.de  
