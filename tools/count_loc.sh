#!/bin/bash
# little bash script to count the lines of code
# needs cloc, found on https://github.com/AlDanial/cloc
cd ..
touch exclude.txt
echo "src/bstrlib.c" > exclude.txt
echo "src/bstrlib.h" >> exclude.txt
echo "src/persistence1d.hpp" >> exclude.txt
echo "docs/doxygen_html/" >> exclude.txt
echo "src/latex/" >> exclude.txt
cloc * --exclude_list_file=exclude.txt --exclude-dir=doxygen_html --exclude-dir=latex
rm exclude.txt
cd tools
