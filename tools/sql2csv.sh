#!/bin/bash
# This little helper reads in the config, executes a sql script within the same directory and 
# gives the results in a .csv file. 

# check if script is called with 6 arguments
if [ $# -lt 6 ]; then
  echo
  echo 1>&2 "$0: a tool to query a mysql database using a sql file and export results to .csv"
  echo "This script is part of the OTraSys trading system framework"
  echo ""
  echo 1>&2 "$0: not enough arguments"
  echo
  echo "Usage: sql2csv.sh -c configfile{.conf} -i inputfile{.sql} -o outputfile{.csv}"
  echo
  echo "ARGUMENTS:"
  echo "-c, --configfile        name of configfile located in ../config, extension .conf is optional and added if missing"
  echo "-i, --inputfile         name of the file containing the SQL query, extension .sql is optional and added if missing"
  echo "-o, --outputfile        name of the csv output file, extension .csv is optional and added if missing"
  echo 
  echo "please send bug reports to d1z@gmx.de"
  echo "please visit https://gitlab.com/werwurm/OTraSys"

  exit 2
elif [ $# -gt 6 ]; then
  echo 1>&2 "$0: too many arguments"
  exit 2
fi

# loop through arguments
while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    # specify config file name
    -c|--configfile)
    CONFIGFILE="../config/"
    CONFIGFILE=$CONFIGFILE"$2"
    # check if extension is missing
    if [ ${CONFIGFILE: -5} != ".conf" ]; then
        CONFIGFILE=$CONFIGFILE".conf"
    fi 
    shift # past argument
    ;;
    #specify file name of sql input
    -i|--inputfile)
    INPUTFILE="$2"
    # check if extension is missing
    if [ ${INPUTFILE: -4} != ".sql" ]; then
        INPUTFILE=$INPUTFILE".sql"
    fi    
    shift # past argument
    ;;
    #specify file name for csv output
    -o|--outputfile)
    OUTPUTFILE="$2"
    # check if extension is missing
    if [ ${OUTPUTFILE: -4} != ".csv" ]; then
        OUTPUTFILE=$OUTPUTFILE".csv"
    fi    
    shift # past argument
    ;;
    --default)
    DEFAULT=YES
    ;;
    *)
    # unknown option
    ;;
esac
shift # past argument or value
done

# argument parsed successfully, print out config for script
echo "exporting database using:"
echo "config file ""${CONFIGFILE}"
echo "query file ""${INPUTFILE}"
echo "and output to ""${OUTPUTFILE}"

# parse config file for db and credentials
user=$(awk '/^DB_SYSTEM_USERNAME/{print $3}' "${CONFIGFILE}") 
pw=$(awk '/^DB_SYSTEM_PASSWORD/{print $3}' "${CONFIGFILE}")    
host=$(awk '/^DB_SYSTEM_HOSTNAME/{print $3}' "${CONFIGFILE}")
db=$(awk '/^DB_SYSTEM_NAME/{print $3}' "${CONFIGFILE}")

# start mysql with user credentials, feed sql script into it and
# feed results through sed to create comma separated records
mysql -u $user -p$pw -h $host $db < ${INPUTFILE} | sed 's/\t/,/g' > ${OUTPUTFILE}
