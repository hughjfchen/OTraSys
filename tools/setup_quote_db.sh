#!/bin/bash
# This script creates the central quote data repository for OTraSys
if [ $# -lt 2 ]; then
    echo
    echo "This script will create the central quote data repository for OTraSys"
    echo "Be aware that all existing data will be lost!" 
    echo
    echo "Usage: setup_quote_db.sh -c configfile{.conf}"
    echo
    echo "ARGUMENTS:"
    echo "-c, --configfile        name of configfile located in ../config, extension .conf is optional and added if missing"
    echo
    echo "This script is part of OTraSys, the Open Trading System"
    echo "please visit https://gitlab.com/werwurm/OTraSys"
    echo
    exit 2
fi
# loop through arguments
while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    # specify config file name
    -c|--configfile)
    CONFIGFILE="../config/"
    CONFIGFILE=$CONFIGFILE"$2"
    # check if extension is missing
    if [ ${CONFIGFILE: -5} != ".conf" ]; then
        CONFIGFILE=$CONFIGFILE".conf"
    fi 
    shift # past argument
    ;;
esac
shift # past argument or value
done

echo
echo "using config file ""${CONFIGFILE}"

user=$(awk '/^DB_QUOTES_USERNAME/{print $3}' "${CONFIGFILE}")  # parse config for 
pw=$(awk '/^DB_QUOTES_PASSWORD/{print $3}' "${CONFIGFILE}")    # db credentials
host=$(awk '/^DB_QUOTES_HOSTNAME/{print $3}' "${CONFIGFILE}")
db=$(awk '/^DB_QUOTES_NAME/{print $3}' "${CONFIGFILE}")

echo
echo "Be aware that this will re-create database ""${db}"" on ""${host}"
echo "Existing data in this database WILL BE LOST! press <CTRL> + <c> to abort now..."
echo "Please enter root user MySQL password! (Note: password hidden while typing)"
read -s mysql_rootpasswd

# now create database and tables
echo "CREATE TABLE \`quotes_daily\` (\`date\` date NOT NULL, \`daynr\` int, \`symbol\` char(40) NOT NULL, \`identifier\` char(20), \`open\` decimal(16,6), \`high\` decimal(16,6), \`low\` decimal(16,6), \`close\` 
decimal(16,6), \`volume\` decimal(16,6), \`returns\` decimal(16,8), PRIMARY KEY (\`date\`,\`symbol\`));" > tmp.sql
mysql -uroot -p${mysql_rootpasswd} -e "DROP DATABASE ${db};"
mysql -uroot -p${mysql_rootpasswd} -e "CREATE DATABASE ${db};"
mysql -uroot -p${mysql_rootpasswd} -h${host} -e "GRANT ALL PRIVILEGES ON  ${db}.* TO '${user}'@'${host}';"
mysql -uroot -p${mysql_rootpasswd} -h${host} -e "FLUSH PRIVILEGES;"
mysql -u${user} -p${pw} -h${host} -D ${db} < tmp.sql
rm tmp.sql
